package Exceptions;

public class AmbienteInexistenteEnListaException extends Exception{
    public AmbienteInexistenteEnListaException(String _mensajeError){
        super(_mensajeError); 
    }
}
