package Exceptions;

public class AmbienteYaExistenteEnListaException extends Exception{
    public AmbienteYaExistenteEnListaException(String _mensajeError){
        super(_mensajeError);
    }
}
