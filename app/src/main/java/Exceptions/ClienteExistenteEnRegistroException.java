package Exceptions;

public class ClienteExistenteEnRegistroException extends Exception{
    public ClienteExistenteEnRegistroException(String _mensajeError){
        super(_mensajeError);
    }
}
