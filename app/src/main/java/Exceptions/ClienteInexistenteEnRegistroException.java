package Exceptions;

public class ClienteInexistenteEnRegistroException extends Exception{
    public ClienteInexistenteEnRegistroException(String _mensajeError){
        super(_mensajeError);
    }
}
