package Exceptions;

public class ComprobanteVacioException extends Exception{
    public ComprobanteVacioException(){
        super("El comprobante no se pudo generar correctamente");
    }
}
