package Exceptions;

public class ContratoDeAlquilerInexistenteException extends Exception{
    public ContratoDeAlquilerInexistenteException(String s){
        super(s);
    }
}
