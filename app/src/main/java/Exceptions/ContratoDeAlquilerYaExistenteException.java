package Exceptions;

public class ContratoDeAlquilerYaExistenteException extends Exception{
    public ContratoDeAlquilerYaExistenteException(String s){
        super(s);
    }
}
