package Exceptions;

public class ContratoDeVentaInexistenteException extends Exception{
    public ContratoDeVentaInexistenteException(String s){
        super(s);
    }
}
