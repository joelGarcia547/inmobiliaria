package Exceptions;

public class ContratoDeVentaYaExistenteException extends Exception{
    public ContratoDeVentaYaExistenteException(String s){
        super(s);
    }
}
