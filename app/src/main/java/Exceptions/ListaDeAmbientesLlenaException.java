package Exceptions;

public class ListaDeAmbientesLlenaException extends Exception{
    public ListaDeAmbientesLlenaException(String _mensajeError){
        super(_mensajeError);
    }
}
