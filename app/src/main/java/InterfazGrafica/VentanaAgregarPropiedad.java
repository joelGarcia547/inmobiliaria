
package InterfazGrafica;

import inmobiliaria.*;
import javax.swing.JOptionPane;

public class VentanaAgregarPropiedad extends javax.swing.JFrame {

    Inmobiliaria inmobiliaria;
    static LibroPropiedad registroPropiedades = new LibroPropiedad();
    
    public VentanaAgregarPropiedad(Inmobiliaria _inmobiliaria) {
        this.inmobiliaria = _inmobiliaria;
        initComponents();
        this.setLocationRelativeTo(null);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupEstadoPropiedad = new javax.swing.ButtonGroup();
        btnGroupTipoPropiedad = new javax.swing.ButtonGroup();
        panelAgregarPropiedad = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblubicacion = new javax.swing.JLabel();
        lblLocalidad = new javax.swing.JLabel();
        lblBarrio = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        lblPrecioPropiedad = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        lblMetrosCuadrados = new javax.swing.JLabel();
        txtLocalidad = new javax.swing.JTextField();
        txtBarrio = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        checkDisponible = new javax.swing.JCheckBox();
        checkVendida = new javax.swing.JCheckBox();
        checkAlquilada = new javax.swing.JCheckBox();
        checkComprada = new javax.swing.JCheckBox();
        txtMetrosCuadrados = new javax.swing.JTextField();
        txtPrecioPropiedad = new javax.swing.JTextField();
        btnAtras = new javax.swing.JButton();
        btnAgregar = new javax.swing.JButton();
        lblTipoPropiedad = new javax.swing.JLabel();
        checkCasa = new javax.swing.JCheckBox();
        checkDepartamento = new javax.swing.JCheckBox();
        checkLocal = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelAgregarPropiedad.setBackground(new java.awt.Color(255, 255, 255));

        lblTitulo.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        lblTitulo.setText("Agregar propiedad");

        lblubicacion.setText("Ubicacion:");

        lblLocalidad.setText("Localidad:");

        lblBarrio.setText("Barrio:");

        lblDireccion.setText("Direccion:");

        lblPrecioPropiedad.setText("Precio de la propiedad:");

        lblEstado.setText("Estado de la propiedad:");

        lblMetrosCuadrados.setText("Metros cuadrados:");

        txtLocalidad.setBackground(new java.awt.Color(204, 204, 204));

        txtBarrio.setBackground(new java.awt.Color(204, 204, 204));

        txtDireccion.setBackground(new java.awt.Color(204, 204, 204));

        btnGroupEstadoPropiedad.add(checkDisponible);
        checkDisponible.setText("Disponible");

        btnGroupEstadoPropiedad.add(checkVendida);
        checkVendida.setText("Vendida");

        btnGroupEstadoPropiedad.add(checkAlquilada);
        checkAlquilada.setText("Alquilada");

        btnGroupEstadoPropiedad.add(checkComprada);
        checkComprada.setText("Comprada");

        txtMetrosCuadrados.setBackground(new java.awt.Color(204, 204, 204));

        txtPrecioPropiedad.setBackground(new java.awt.Color(204, 204, 204));

        btnAtras.setBackground(new java.awt.Color(204, 204, 204));
        btnAtras.setText("Atras");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        btnAgregar.setBackground(new java.awt.Color(204, 204, 204));
        btnAgregar.setText("Agregar");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        lblTipoPropiedad.setText("La propiedad es una/un:");

        btnGroupTipoPropiedad.add(checkCasa);
        checkCasa.setText("Casa");

        btnGroupTipoPropiedad.add(checkDepartamento);
        checkDepartamento.setText("Departamento");

        btnGroupTipoPropiedad.add(checkLocal);
        checkLocal.setText("Local comercial");

        javax.swing.GroupLayout panelAgregarPropiedadLayout = new javax.swing.GroupLayout(panelAgregarPropiedad);
        panelAgregarPropiedad.setLayout(panelAgregarPropiedadLayout);
        panelAgregarPropiedadLayout.setHorizontalGroup(
            panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                        .addComponent(lblubicacion)
                        .addGap(18, 18, 18)
                        .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblLocalidad)
                            .addComponent(lblBarrio)
                            .addComponent(lblDireccion))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtLocalidad)
                            .addComponent(txtDireccion)
                            .addComponent(txtBarrio)))
                    .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                        .addComponent(lblMetrosCuadrados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtMetrosCuadrados))
                    .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                        .addComponent(lblPrecioPropiedad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPrecioPropiedad))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAgregarPropiedadLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAgregar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAtras))
                    .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                        .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTitulo)
                            .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                                .addComponent(lblEstado)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(checkDisponible)
                                .addGap(18, 18, 18)
                                .addComponent(checkVendida)
                                .addGap(18, 18, 18)
                                .addComponent(checkAlquilada)
                                .addGap(18, 18, 18)
                                .addComponent(checkComprada))
                            .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                                .addComponent(lblTipoPropiedad, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkCasa)
                                .addGap(18, 18, 18)
                                .addComponent(checkDepartamento)
                                .addGap(18, 18, 18)
                                .addComponent(checkLocal)))
                        .addGap(0, 102, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelAgregarPropiedadLayout.setVerticalGroup(
            panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblubicacion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBarrio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBarrio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkDisponible, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkVendida, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkAlquilada, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkComprada, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblMetrosCuadrados, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAgregarPropiedadLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(txtMetrosCuadrados)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPrecioPropiedad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrecioPropiedad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTipoPropiedad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkCasa, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkLocal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(panelAgregarPropiedadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAtras)
                    .addComponent(btnAgregar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAgregarPropiedad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAgregarPropiedad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        VentanaGestionPropiedades ventanaGestionPropiedades = new VentanaGestionPropiedades(inmobiliaria);
        ventanaGestionPropiedades.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
     
        String localidad = txtLocalidad.getText().toUpperCase();
        String barrio = txtBarrio.getText().toUpperCase();
        String direccion = txtDireccion.getText().toUpperCase();
        Ubicacion ubicacionPropiedad = new Ubicacion(localidad, barrio, direccion);
        Float metrosCuadrados = Float.valueOf(txtMetrosCuadrados.getText());
        Float precioPropiedad = Float.valueOf(txtPrecioPropiedad.getText());
        String estadoPropiedad = "Disponible";
        if(checkDisponible.isSelected()){
            estadoPropiedad = "Disponible";
        }else{
            if(checkVendida.isSelected()){
                estadoPropiedad = "Vendida";
            }else{
                if(checkComprada.isSelected()){
                    estadoPropiedad = "Comprada";
                }else{
                    if(checkAlquilada.isSelected()){
                        estadoPropiedad = "Alquilada";
                    }else{
                        JOptionPane.showMessageDialog(rootPane, "Por favor seleccione un estado para la propiedad", "Alerta",0);
                    }
                }
            }
        }
        if(checkCasa.isSelected()){
            VentanaDeCasa ventanaCasa = new VentanaDeCasa(inmobiliaria, registroPropiedades, ubicacionPropiedad, metrosCuadrados, estadoPropiedad, precioPropiedad);
            ventanaCasa.setVisible(true);
        }else{
            if(checkDepartamento.isSelected()){
                VentanaDeDepartamento ventanaDpto = new VentanaDeDepartamento(inmobiliaria, registroPropiedades, ubicacionPropiedad, metrosCuadrados, estadoPropiedad, precioPropiedad);
                ventanaDpto.setVisible(true);
            } else{
                if(checkLocal.isSelected()){
                    VentanaDeLocalComercial ventanaLocalComercial = new VentanaDeLocalComercial(inmobiliaria, registroPropiedades, ubicacionPropiedad, metrosCuadrados, estadoPropiedad, precioPropiedad);
                    ventanaLocalComercial.setVisible(true);
                }else{
                    JOptionPane.showMessageDialog(rootPane, "Por favor seleccione un tipo de propiedad", "Alerta",0);
                }
            }
        }
    }//GEN-LAST:event_btnAgregarActionPerformed


//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaAgregarPropiedad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaAgregarPropiedad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaAgregarPropiedad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaAgregarPropiedad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new VentanaAgregarPropiedad().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnAtras;
    private javax.swing.ButtonGroup btnGroupEstadoPropiedad;
    private javax.swing.ButtonGroup btnGroupTipoPropiedad;
    private javax.swing.JCheckBox checkAlquilada;
    private javax.swing.JCheckBox checkCasa;
    private javax.swing.JCheckBox checkComprada;
    private javax.swing.JCheckBox checkDepartamento;
    private javax.swing.JCheckBox checkDisponible;
    private javax.swing.JCheckBox checkLocal;
    private javax.swing.JCheckBox checkVendida;
    private javax.swing.JLabel lblBarrio;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblLocalidad;
    private javax.swing.JLabel lblMetrosCuadrados;
    private javax.swing.JLabel lblPrecioPropiedad;
    private javax.swing.JLabel lblTipoPropiedad;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblubicacion;
    private javax.swing.JPanel panelAgregarPropiedad;
    private javax.swing.JTextField txtBarrio;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtLocalidad;
    private javax.swing.JTextField txtMetrosCuadrados;
    private javax.swing.JTextField txtPrecioPropiedad;
    // End of variables declaration//GEN-END:variables
}
