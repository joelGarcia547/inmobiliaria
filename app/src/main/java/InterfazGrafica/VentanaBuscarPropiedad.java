
package InterfazGrafica;
import inmobiliaria.*;
import java.util.List;
import javax.swing.*;

public class VentanaBuscarPropiedad extends javax.swing.JFrame {

    Inmobiliaria inmobiliaria;
    DefaultListModel listaDeCasas = new DefaultListModel();
    DefaultListModel listaDeDptos = new DefaultListModel();
    DefaultListModel listaDeLocales = new DefaultListModel();
    public VentanaBuscarPropiedad(Inmobiliaria _inmobiliaria) {
        this.inmobiliaria = _inmobiliaria;
        initComponents();
        this.setLocationRelativeTo(null);
        verificarListaVacia();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBuscar = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listCasas = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        listDptos = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        listLocalesComerciales = new javax.swing.JList<>();
        btnAtras = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelBuscar.setBackground(new java.awt.Color(255, 255, 255));

        lblTitulo.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        lblTitulo.setText("Buscar propiedad");

        jLabel2.setText("Localidad");

        jScrollPane1.setViewportView(listCasas);

        jScrollPane2.setViewportView(listDptos);

        jLabel1.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel1.setText("Casas");

        jLabel3.setText("Localidad");

        jLabel4.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel4.setText("Dptos");

        jLabel5.setText("Barrio");

        jLabel6.setText("Direccion");

        jLabel7.setText("Estado");

        jLabel8.setText("Metros cuadrados");

        jLabel9.setText("Precio");

        jLabel10.setText("Barrio");

        jLabel11.setText("Direccion");

        jLabel12.setText("Estado");

        jLabel13.setText("Metros cuadrados");

        jLabel14.setText("Precio");

        jLabel15.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel15.setText("Local comercial");

        jLabel16.setText("Localidad");

        jLabel17.setText("Barrio");

        jLabel18.setText("Direccion");

        jLabel19.setText("Estado");

        jLabel20.setText("Metros cuadrados");

        jLabel21.setText("Precio");

        jScrollPane3.setViewportView(listLocalesComerciales);

        btnAtras.setText("Atras");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar propiedad");
        btnEliminar.setEnabled(false);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnModificar.setText("Modificar propiedad");
        btnModificar.setEnabled(false);
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBuscarLayout = new javax.swing.GroupLayout(panelBuscar);
        panelBuscar.setLayout(panelBuscarLayout);
        panelBuscarLayout.setHorizontalGroup(
            panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBuscarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitulo, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelBuscarLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(61, 61, 61)
                        .addComponent(jLabel5)
                        .addGap(57, 57, 57)
                        .addComponent(jLabel6)
                        .addGap(62, 62, 62)
                        .addComponent(jLabel7)
                        .addGap(65, 65, 65)
                        .addComponent(jLabel8)
                        .addGap(62, 62, 62)
                        .addComponent(jLabel9))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelBuscarLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(61, 61, 61)
                        .addComponent(jLabel10)
                        .addGap(61, 61, 61)
                        .addComponent(jLabel11)
                        .addGap(64, 64, 64)
                        .addComponent(jLabel12)
                        .addGap(63, 63, 63)
                        .addComponent(jLabel13)
                        .addGap(59, 59, 59)
                        .addComponent(jLabel14))
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelBuscarLayout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addGap(58, 58, 58)
                        .addComponent(jLabel17)
                        .addGap(63, 63, 63)
                        .addComponent(jLabel18)
                        .addGap(67, 67, 67)
                        .addComponent(jLabel19)
                        .addGap(66, 66, 66)
                        .addComponent(jLabel20)
                        .addGap(60, 60, 60)
                        .addComponent(jLabel21))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap(94, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBuscarLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnModificar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAtras)
                .addContainerGap())
        );
        panelBuscarLayout.setVerticalGroup(
            panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBuscarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificar)
                    .addComponent(btnEliminar)
                    .addComponent(btnAtras))
                .addGap(17, 17, 17))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 643, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void verificarListaVacia(){
        if(inmobiliaria.getRegistroDePropiedades() == null){
            JOptionPane.showMessageDialog(rootPane, "Error, no se agrego ninguna propiedad a la lista", "Alerta",0);
        }else{
            LibroPropiedad listaDePropiedades = inmobiliaria.getRegistroDePropiedades();
            List<Propiedad> registroPropiedades = listaDePropiedades.getRegistroDePropiedades();
            Boolean registroVacio = false;
            if(registroPropiedades.isEmpty()){
                JOptionPane.showMessageDialog(rootPane, "Error, no se agrego ninguna propiedad a la lista", "Alerta",0);
            }else{
                llenarListaDeCasas();
                llenarListaDeDpto();
                llenarListaDeLocal();
                //agregarlistadeDptos y locales
            }
        }
    }
    private void llenarListaDeCasas(){
        listCasas.setModel(listaDeCasas);
        listaDeCasas.removeAllElements();
        LibroPropiedad listaDePropiedades = inmobiliaria.getRegistroDePropiedades();
        
        for(int i = 0; i<listaDePropiedades.getRegistroDePropiedades().size(); i++){
            if(listaDePropiedades.getRegistroDePropiedades().get(i) instanceof Casa){
                Casa casa = (Casa) listaDePropiedades.getRegistroDePropiedades().get(i);
                Ubicacion ubicacionCasa = listaDePropiedades.getRegistroDePropiedades().get(i).getUbicacionDePropiedad();
                String localidad = ubicacionCasa.getLocalidad();
                String barrio = ubicacionCasa.getBarrio();
                String direccion = ubicacionCasa.getDireccion();
                String estadoCasa = listaDePropiedades.getRegistroDePropiedades().get(i).getEstadoDeLaPropiedad();
                Float metrosCuadradosCasa = listaDePropiedades.getRegistroDePropiedades().get(i).getMetrosCuadradosDeLaPropiedad();
                Float precioCasa = listaDePropiedades.getRegistroDePropiedades().get(i).getPrecioDePropiedad();
                listaDeCasas.addElement(localidad + "   -   " + barrio+ "   -   "+ direccion+ "   |   "+estadoCasa+ "   |   " + metrosCuadradosCasa+ "   |   "+precioCasa);
            }
        }
    }
    private void llenarListaDeDpto(){
        listDptos.setModel(listaDeDptos);
        listaDeDptos.removeAllElements();
        LibroPropiedad listaDePropiedades = inmobiliaria.getRegistroDePropiedades();
        for(int i = 0; i<listaDePropiedades.getRegistroDePropiedades().size(); i++){
            if(listaDePropiedades.getRegistroDePropiedades().get(i) instanceof Departamento){
                Departamento departamento = (Departamento) listaDePropiedades.getRegistroDePropiedades().get(i);
                Ubicacion ubicacionCasa = listaDePropiedades.getRegistroDePropiedades().get(i).getUbicacionDePropiedad();
                String localidad = ubicacionCasa.getLocalidad();
                String barrio = ubicacionCasa.getBarrio();
                String direccion = ubicacionCasa.getDireccion();
                String estadoDpto = listaDePropiedades.getRegistroDePropiedades().get(i).getEstadoDeLaPropiedad();
                Float metrosCuadradosDpto = listaDePropiedades.getRegistroDePropiedades().get(i).getMetrosCuadradosDeLaPropiedad();
                Float precioDpto = listaDePropiedades.getRegistroDePropiedades().get(i).getPrecioDePropiedad();
                listaDeDptos.addElement(localidad + "   -   " + barrio+ "   -   "+ direccion+ "   |   "+estadoDpto+ "   |   " + metrosCuadradosDpto+ "   |   "+precioDpto);
            }
        }
    }
    private void llenarListaDeLocal(){
        listLocalesComerciales.setModel(listaDeLocales);
        listaDeLocales.removeAllElements();
        LibroPropiedad listaDePropiedades = inmobiliaria.getRegistroDePropiedades();
        for(int i = 0; i<listaDePropiedades.getRegistroDePropiedades().size(); i++){
            if(listaDePropiedades.getRegistroDePropiedades().get(i) instanceof LocalComercial){
                LocalComercial localComercial = (LocalComercial) listaDePropiedades.getRegistroDePropiedades().get(i);
                Ubicacion ubicacionCasa = listaDePropiedades.getRegistroDePropiedades().get(i).getUbicacionDePropiedad();
                String localidad = ubicacionCasa.getLocalidad();
                String barrio = ubicacionCasa.getBarrio();
                String direccion = ubicacionCasa.getDireccion();
                String estadoLocal = listaDePropiedades.getRegistroDePropiedades().get(i).getEstadoDeLaPropiedad();
                Float metrosCuadradosLocal = listaDePropiedades.getRegistroDePropiedades().get(i).getMetrosCuadradosDeLaPropiedad();
                Float precioLocal = listaDePropiedades.getRegistroDePropiedades().get(i).getPrecioDePropiedad();
                listaDeLocales.addElement(localidad + "   -   " + barrio+ "   -   "+ direccion+ "   |   "+estadoLocal+ "   |   " + metrosCuadradosLocal+ "   |   "+precioLocal);
            }
        }
    }
    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        VentanaGestionPropiedades ventanaGestionPropiedades = new VentanaGestionPropiedades(inmobiliaria);
        ventanaGestionPropiedades.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        
    }//GEN-LAST:event_btnModificarActionPerformed
    

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaBuscarPropiedad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaBuscarPropiedad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaBuscarPropiedad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaBuscarPropiedad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new VentanaBuscarPropiedad().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JList<String> listCasas;
    private javax.swing.JList<String> listDptos;
    private javax.swing.JList<String> listLocalesComerciales;
    private javax.swing.JPanel panelBuscar;
    // End of variables declaration//GEN-END:variables
}
