
package InterfazGrafica;

import inmobiliaria.*;
import Exceptions.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class VentanaDeCasa extends javax.swing.JFrame {

    Inmobiliaria inmobiliaria;
    LibroPropiedad registroPropiedades;
    Ubicacion ubicacionCasa;
    Float metrosCuadradosCasa;
    Float precioCasa;
    String estadoCasa;
    Integer cantidadAmbientes;
    Casa casa;
    Integer cantidad = 0;
    public VentanaDeCasa(Inmobiliaria _inmobiliaria, LibroPropiedad _registroPropiedades, Ubicacion _ubicacionPropiedad,Float _metrosCuadrados,String _estado, Float _precio ) {
        this.inmobiliaria = _inmobiliaria;
        this.registroPropiedades = _registroPropiedades;
        this.ubicacionCasa = _ubicacionPropiedad;
        this.metrosCuadradosCasa = _metrosCuadrados;
        this.estadoCasa = _estado;
        this.precioCasa = _precio;
        initComponents();
        this.setLocationRelativeTo(null);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelCasa = new javax.swing.JPanel();
        lblCasa = new javax.swing.JLabel();
        lblCantidadAmbientes = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        btnAtras = new javax.swing.JButton();
        txtNombreAmbiente = new javax.swing.JTextField();
        btnCargar = new javax.swing.JButton();
        lblCantidad = new javax.swing.JLabel();
        lblCantidadFinal = new javax.swing.JLabel();
        btnAceptar = new javax.swing.JButton();
        btnCantidad = new javax.swing.JButton();
        txtMetrosCuadradosAmbiente = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelCasa.setBackground(new java.awt.Color(204, 204, 255));

        lblCasa.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        lblCasa.setText("Casa");

        lblCantidadAmbientes.setText("Cantidad de Ambientes:");

        btnAtras.setBackground(new java.awt.Color(153, 153, 153));
        btnAtras.setText("Atras");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        txtNombreAmbiente.setText("Ingrese nombre del ambiente");
        txtNombreAmbiente.setEnabled(false);

        btnCargar.setBackground(new java.awt.Color(0, 102, 102));
        btnCargar.setForeground(new java.awt.Color(255, 255, 255));
        btnCargar.setText("Cargar");
        btnCargar.setEnabled(false);
        btnCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarActionPerformed(evt);
            }
        });

        lblCantidad.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCantidad.setText("0");

        btnAceptar.setText("Aceptar");
        btnAceptar.setEnabled(false);
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCantidad.setBackground(new java.awt.Color(0, 102, 102));
        btnCantidad.setForeground(new java.awt.Color(255, 255, 255));
        btnCantidad.setText("Confirmar");
        btnCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCantidadActionPerformed(evt);
            }
        });

        txtMetrosCuadradosAmbiente.setText("Ingrese los metros cuadrados del ambiente");
        txtMetrosCuadradosAmbiente.setEnabled(false);

        jLabel1.setText("/");

        javax.swing.GroupLayout panelCasaLayout = new javax.swing.GroupLayout(panelCasa);
        panelCasa.setLayout(panelCasaLayout);
        panelCasaLayout.setHorizontalGroup(
            panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCasaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCasaLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAceptar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAtras))
                    .addGroup(panelCasaLayout.createSequentialGroup()
                        .addGroup(panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCasa)
                            .addGroup(panelCasaLayout.createSequentialGroup()
                                .addComponent(lblCantidadAmbientes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCantidad))
                            .addGroup(panelCasaLayout.createSequentialGroup()
                                .addGroup(panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNombreAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtMetrosCuadradosAmbiente, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCargar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblCantidadFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 76, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelCasaLayout.setVerticalGroup(
            panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCasaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCasa, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCantidadAmbientes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelCasaLayout.createSequentialGroup()
                        .addComponent(txtNombreAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtMetrosCuadradosAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCargar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(lblCantidadFinal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(panelCasaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnAtras))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelCasa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelCasa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCantidadActionPerformed
        cantidadAmbientes = Integer.valueOf(txtCantidad.getText());
        if(cantidadAmbientes > 0){
            lblCantidadFinal.setText(txtCantidad.getText());
            txtCantidad.setEnabled(false);
            btnCargar.setEnabled(true);
            txtNombreAmbiente.setEnabled(true);
            txtMetrosCuadradosAmbiente.setEnabled(true);
        
            Casa _casa = new Casa(cantidadAmbientes, ubicacionCasa, metrosCuadradosCasa, estadoCasa, precioCasa);
            casa = _casa;
        }else{
            JOptionPane.showMessageDialog(rootPane, "Error, Cantidad de ambientes invalida", "Alerta",0);
        }
        
    }//GEN-LAST:event_btnCantidadActionPerformed

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        dispose();
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        String nombreAmbiente = txtNombreAmbiente.getText().toUpperCase();
        Float metrosCuadradosAmbiente = Float.valueOf(txtMetrosCuadradosAmbiente.getText());
        Ambiente ambienteDeLaCasa = new Ambiente(nombreAmbiente, metrosCuadradosAmbiente);
        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambienteDeLaCasa);
            JOptionPane.showMessageDialog(rootPane, "Ambiente agregado a la casa");
            cantidad = cantidad +1;
            lblCantidad.setText(String.valueOf(cantidad));
        } catch (ListaDeAmbientesLlenaException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(rootPane, "Error, Cantidad de ambientes completada", "Alerta",0);
        }
        if(cantidad.equals(cantidadAmbientes)){
            btnAceptar.setEnabled(true);
        }
    }//GEN-LAST:event_btnCargarActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        try{
            registroPropiedades.agregarPropiedadALibro(casa);
            inmobiliaria.setLibroPropiedades(registroPropiedades);
            JOptionPane.showMessageDialog(rootPane, "Casa agregada al registro");
            dispose();
        } catch(PropiedadRepetidaException e){
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(rootPane, "Error, esta propiedad ya fue agregada anteriormente", "Alerta",0);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeCasa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeCasa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeCasa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeCasa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new VentanaDeCasa().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnCantidad;
    private javax.swing.JButton btnCargar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblCantidad;
    private javax.swing.JLabel lblCantidadAmbientes;
    private javax.swing.JLabel lblCantidadFinal;
    private javax.swing.JLabel lblCasa;
    private javax.swing.JPanel panelCasa;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtMetrosCuadradosAmbiente;
    private javax.swing.JTextField txtNombreAmbiente;
    // End of variables declaration//GEN-END:variables
}
