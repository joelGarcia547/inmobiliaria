
package InterfazGrafica;
import inmobiliaria.*;
import Exceptions.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class VentanaDeDepartamento extends javax.swing.JFrame {

    Inmobiliaria inmobiliaria;
    LibroPropiedad registroPropiedades;
    Ubicacion ubicacionDpto;
    Float metrosCuadradosDpto;
    Float precioDpto;
    String estadoDpto;
    Integer cantidadAmbientes;
    Integer pisoDelDpto;
    Integer numeroDelDpto;
    Boolean cocheraDpto;
    Departamento departamento;
    Integer cantidad = 0;
    public VentanaDeDepartamento(Inmobiliaria _inmobiliaria, LibroPropiedad _registroPropiedades, Ubicacion _ubicacionDpto, Float _metrosCuadradosDpto, String _estadoPropiedad, Float _precioDpto) {
        this.inmobiliaria = _inmobiliaria;
        this.registroPropiedades = _registroPropiedades;
        this.ubicacionDpto = _ubicacionDpto;
        this.metrosCuadradosDpto = _metrosCuadradosDpto;
        this.precioDpto = _precioDpto;
        this.estadoDpto = _estadoPropiedad;
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        panelDepartamento = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblCantidadAmbientes = new javax.swing.JLabel();
        txtCantidadAmbientes = new javax.swing.JTextField();
        btnConfirmar = new javax.swing.JButton();
        txtNombreAmbiente = new javax.swing.JTextField();
        txtMetrosCuadrados = new javax.swing.JTextField();
        btnCargar = new javax.swing.JButton();
        txtPisoDpto = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNumeroDpto = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        checkSi = new javax.swing.JCheckBox();
        checkNo = new javax.swing.JCheckBox();
        lblCantidadInicial = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblCantidadFinal = new javax.swing.JLabel();
        btnAtras = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelDepartamento.setBackground(new java.awt.Color(204, 204, 255));

        lblTitulo.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        lblTitulo.setText("Departamento");

        lblCantidadAmbientes.setText("Cantidad de ambientes:");

        btnConfirmar.setBackground(new java.awt.Color(0, 102, 102));
        btnConfirmar.setForeground(new java.awt.Color(255, 255, 255));
        btnConfirmar.setText("Confirmar");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        txtNombreAmbiente.setText("Ingrese nombre del ambiente");
        txtNombreAmbiente.setEnabled(false);

        txtMetrosCuadrados.setText("Ingrese metros cuadrados del ambiente");
        txtMetrosCuadrados.setEnabled(false);

        btnCargar.setBackground(new java.awt.Color(0, 102, 102));
        btnCargar.setForeground(new java.awt.Color(255, 255, 255));
        btnCargar.setText("Cargar");
        btnCargar.setEnabled(false);
        btnCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarActionPerformed(evt);
            }
        });

        txtPisoDpto.setText("Ingrese piso del departamento");

        jLabel1.setText("Nombre del ambiente:");

        jLabel2.setText("Metros cuadrados del ambiente:");

        jLabel3.setText("Piso Dpto:");

        jLabel4.setText("N� Dpto:");

        txtNumeroDpto.setText("Ingrese N� del departamento");

        jLabel5.setText("Cochera:");

        checkSi.setBackground(new java.awt.Color(204, 204, 255));
        buttonGroup1.add(checkSi);
        checkSi.setText("Si");

        checkNo.setBackground(new java.awt.Color(204, 204, 255));
        buttonGroup1.add(checkNo);
        checkNo.setText("No");

        lblCantidadInicial.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCantidadInicial.setText("0");

        jLabel6.setText("/");

        lblCantidadFinal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        btnAtras.setBackground(new java.awt.Color(153, 153, 153));
        btnAtras.setText("Atras");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        btnAceptar.setBackground(new java.awt.Color(153, 153, 153));
        btnAceptar.setText("Aceptar");
        btnAceptar.setEnabled(false);
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelDepartamentoLayout = new javax.swing.GroupLayout(panelDepartamento);
        panelDepartamento.setLayout(panelDepartamentoLayout);
        panelDepartamentoLayout.setHorizontalGroup(
            panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDepartamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDepartamentoLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNombreAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panelDepartamentoLayout.createSequentialGroup()
                        .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnAceptar)
                            .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblTitulo)
                                .addGroup(panelDepartamentoLayout.createSequentialGroup()
                                    .addComponent(lblCantidadAmbientes)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtCantidadAmbientes, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(panelDepartamentoLayout.createSequentialGroup()
                                    .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(btnCargar)
                                        .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDepartamentoLayout.createSequentialGroup()
                                                    .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDepartamentoLayout.createSequentialGroup()
                                                            .addComponent(jLabel5)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(checkSi)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(checkNo))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDepartamentoLayout.createSequentialGroup()
                                                            .addComponent(jLabel4)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(txtNumeroDpto, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(btnConfirmar))
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelDepartamentoLayout.createSequentialGroup()
                                                    .addComponent(jLabel3)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(txtPisoDpto, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(panelDepartamentoLayout.createSequentialGroup()
                                                .addComponent(jLabel2)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtMetrosCuadrados, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lblCantidadInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel6)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lblCantidadFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                        .addComponent(btnAtras)
                        .addContainerGap())))
        );
        panelDepartamentoLayout.setVerticalGroup(
            panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDepartamentoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCantidadAmbientes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCantidadAmbientes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPisoDpto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNumeroDpto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkSi, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkNo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMetrosCuadrados, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnCargar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblCantidadInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblCantidadFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelDepartamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnAtras))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelDepartamento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelDepartamento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        dispose();
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
        cantidadAmbientes = Integer.valueOf(txtCantidadAmbientes.getText().trim());
        pisoDelDpto = Integer.valueOf(txtPisoDpto.getText().trim());
        numeroDelDpto = Integer.valueOf(txtNumeroDpto.getText().trim());
        Boolean cochera = false;
        if(checkSi.isSelected()){
            cochera = true;
        }else{
            if(checkNo.isSelected()){
                cochera = false;
            }else{
                JOptionPane.showMessageDialog(rootPane, "Error, seleccione si tiene cochera o no", "Alerta",0);
            }
        }
        if(cantidadAmbientes > 0){
            if(checkSi.isSelected() || checkNo.isSelected()){
                checkSi.setEnabled(false);
                checkNo.setEnabled(false);
                txtNombreAmbiente.setEnabled(true);
                txtMetrosCuadrados.setEnabled(true);
                btnCargar.setEnabled(true);
                lblCantidadFinal.setText(String.valueOf(cantidadAmbientes));
                txtCantidadAmbientes.setEnabled(false);
                txtPisoDpto.setEnabled(false);
                txtNumeroDpto.setEnabled(false);
                btnConfirmar.setEnabled(false);
                departamento = new Departamento(pisoDelDpto, numeroDelDpto, cochera, cantidadAmbientes, ubicacionDpto, metrosCuadradosDpto, estadoDpto, precioDpto);
            }else{
                JOptionPane.showMessageDialog(rootPane, "Error, seleccione si tiene cochera o no", "Alerta",0);
            }
        }else{
            JOptionPane.showMessageDialog(rootPane, "Error, Cantidad de ambientes invalida", "Alerta",0);
        }
    }//GEN-LAST:event_btnConfirmarActionPerformed

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        String nombreAmbiente = txtNombreAmbiente.getText().toUpperCase();
        Float metrosCuadradosAmbiente = Float.valueOf(txtMetrosCuadrados.getText().toUpperCase().trim());
        Ambiente ambiente = new Ambiente(nombreAmbiente, metrosCuadradosAmbiente);
        try{
            departamento.agregarAmbienteAlDpto(ambiente);
            JOptionPane.showMessageDialog(rootPane, "Ambiente agregado al dpto");
            cantidad = cantidad +1;
            lblCantidadInicial.setText(String.valueOf(cantidad));
        } catch(ListaDeAmbientesLlenaException e){
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(rootPane, "Error, cantidad de ambientes del dpto completada", "Alerta",0);
        }
        if(cantidad.equals(cantidadAmbientes)){
            btnAceptar.setEnabled(true);
        }
    }//GEN-LAST:event_btnCargarActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        try {
            registroPropiedades.agregarPropiedadALibro(departamento);
            inmobiliaria.setLibroPropiedades(registroPropiedades);
            JOptionPane.showMessageDialog(rootPane, "Departamento agregado al registro");
            dispose();
        } catch (PropiedadRepetidaException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(rootPane, "Error, esta propiedad ya fue agregada anteriormente", "Alerta",0);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeDepartamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeDepartamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeDepartamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeDepartamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new VentanaDeDepartamento().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnCargar;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox checkNo;
    private javax.swing.JCheckBox checkSi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel lblCantidadAmbientes;
    private javax.swing.JLabel lblCantidadFinal;
    private javax.swing.JLabel lblCantidadInicial;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JPanel panelDepartamento;
    private javax.swing.JTextField txtCantidadAmbientes;
    private javax.swing.JTextField txtMetrosCuadrados;
    private javax.swing.JTextField txtNombreAmbiente;
    private javax.swing.JTextField txtNumeroDpto;
    private javax.swing.JTextField txtPisoDpto;
    // End of variables declaration//GEN-END:variables
}
