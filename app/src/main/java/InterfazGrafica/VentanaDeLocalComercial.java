
package InterfazGrafica;
import inmobiliaria.*;
import Exceptions.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class VentanaDeLocalComercial extends javax.swing.JFrame {

    Inmobiliaria inmobiliaria;
    LibroPropiedad registroPropiedades;
    Ubicacion ubicacionDelLocal;
    Float metrosCuadradosPropiedad;
    String estadoDelLocal;
    Float precioDelLocal;
    Float metrosCuadradosDeVidriera = null;
    Boolean poseeBanio = null;
    Boolean poseeCocina = null;
    public VentanaDeLocalComercial(Inmobiliaria _inmobiliaria, LibroPropiedad _registroPropiedades, Ubicacion _ubicacionDelLocal, Float _metrosCuadrados, String _estadoDelLocal, Float _precioDelLocal) {
        this.inmobiliaria = _inmobiliaria;
        this.registroPropiedades = _registroPropiedades;
        this.ubicacionDelLocal = _ubicacionDelLocal;
        this.metrosCuadradosPropiedad = _metrosCuadrados;
        this.estadoDelLocal = _estadoDelLocal;
        this.precioDelLocal = _precioDelLocal;
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        panelLocalComercial = new javax.swing.JPanel();
        btnAtras = new javax.swing.JButton();
        lblTitulo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtMetrosVidriera = new javax.swing.JTextField();
        checkSi = new javax.swing.JCheckBox();
        checkNo = new javax.swing.JCheckBox();
        checkSi2 = new javax.swing.JCheckBox();
        checkNo2 = new javax.swing.JCheckBox();
        btnAceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelLocalComercial.setBackground(new java.awt.Color(204, 204, 255));

        btnAtras.setBackground(new java.awt.Color(153, 153, 153));
        btnAtras.setText("Atras");

        lblTitulo.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        lblTitulo.setText("Local comercial");

        jLabel1.setText("Metros cuadrados de vidriera:");

        jLabel2.setText("Posee ba�o:");

        jLabel3.setText("Posee cocina:");

        buttonGroup1.add(checkSi);
        checkSi.setText("Si");

        buttonGroup1.add(checkNo);
        checkNo.setText("No");

        buttonGroup2.add(checkSi2);
        checkSi2.setText("Si");

        buttonGroup2.add(checkNo2);
        checkNo2.setText("No");

        btnAceptar.setBackground(new java.awt.Color(153, 153, 153));
        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelLocalComercialLayout = new javax.swing.GroupLayout(panelLocalComercial);
        panelLocalComercial.setLayout(panelLocalComercialLayout);
        panelLocalComercialLayout.setHorizontalGroup(
            panelLocalComercialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLocalComercialLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelLocalComercialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLocalComercialLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAceptar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAtras))
                    .addGroup(panelLocalComercialLayout.createSequentialGroup()
                        .addGroup(panelLocalComercialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTitulo)
                            .addGroup(panelLocalComercialLayout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(checkSi2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(checkNo2))
                            .addGroup(panelLocalComercialLayout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkSi)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(checkNo))
                            .addGroup(panelLocalComercialLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMetrosVidriera, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 303, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelLocalComercialLayout.setVerticalGroup(
            panelLocalComercialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLocalComercialLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelLocalComercialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMetrosVidriera, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLocalComercialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkSi, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkNo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelLocalComercialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkSi2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkNo2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(panelLocalComercialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAtras)
                    .addComponent(btnAceptar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelLocalComercial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelLocalComercial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        metrosCuadradosDeVidriera = Float.valueOf(txtMetrosVidriera.getText().trim());
        //chequeamos el ba�o primero
        if(checkSi.isSelected()){
            poseeBanio = true;
        }else{
            if(checkNo.isSelected()){
                poseeBanio = false;
            }else{
                JOptionPane.showMessageDialog(rootPane, "Por favor seleccione si tiene ba�o o no", "Alerta",0);
            }
        }
        //chequeamos cocina
        if(checkSi2.isSelected()){
            poseeCocina = true;
        }else{
            if(checkNo2.isSelected()){
                poseeCocina = false;
            }else{
                JOptionPane.showMessageDialog(rootPane, "Por favor seleccione si tiene cocina o no", "Alerta",0);
            }
        }
        if(metrosCuadradosDeVidriera >= 0){
            if(poseeBanio != null && poseeCocina != null){
                LocalComercial localComercial = new LocalComercial(metrosCuadradosDeVidriera, poseeBanio, poseeCocina, ubicacionDelLocal, metrosCuadradosPropiedad, estadoDelLocal, precioDelLocal);
                try {
                    registroPropiedades.agregarPropiedadALibro(localComercial);
                    inmobiliaria.setLibroPropiedades(registroPropiedades);
                    JOptionPane.showMessageDialog(rootPane, "Local comercial agregado al registro");
                    dispose();
                } catch (PropiedadRepetidaException ex) {
                    System.out.println(ex.getMessage());
                    JOptionPane.showMessageDialog(rootPane, "Error, esta propiedad ya fue agregada anteriormente", "Alerta",0);
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane, "Por favor ingrese los metros cuadrados de vidriera", "Alerta",0);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeLocalComercial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeLocalComercial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeLocalComercial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaDeLocalComercial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new VentanaDeLocalComercial().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnAtras;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JCheckBox checkNo;
    private javax.swing.JCheckBox checkNo2;
    private javax.swing.JCheckBox checkSi;
    private javax.swing.JCheckBox checkSi2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JPanel panelLocalComercial;
    private javax.swing.JTextField txtMetrosVidriera;
    // End of variables declaration//GEN-END:variables
}
