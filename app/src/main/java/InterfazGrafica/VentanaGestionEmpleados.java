
package InterfazGrafica;
import inmobiliaria.*;
import Exceptions.*;
import javax.swing.JOptionPane;

public class VentanaGestionEmpleados extends javax.swing.JFrame {

    Inmobiliaria inmobiliaria;
    public VentanaGestionEmpleados(Inmobiliaria _inmobiliaria) {
        this.inmobiliaria = _inmobiliaria;
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelGestionEmpleados = new javax.swing.JPanel();
        btnAtras = new javax.swing.JButton();
        lblTitulo = new javax.swing.JLabel();
        btnAgregarEmpleado = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnEliminarEmpleado = new javax.swing.JButton();
        txtBuscarEmpleado = new javax.swing.JTextField();
        lblapellido = new javax.swing.JLabel();
        lblApellido = new javax.swing.JLabel();
        lblnombre = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lbldni = new javax.swing.JLabel();
        lblDni = new javax.swing.JLabel();
        lblcorreo = new javax.swing.JLabel();
        lblCorreo = new javax.swing.JLabel();
        lbltelefono = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        lblareaTrabajo = new javax.swing.JLabel();
        lblArea = new javax.swing.JLabel();
        lbldomicilio = new javax.swing.JLabel();
        lbllocalidad = new javax.swing.JLabel();
        lblLocalidad = new javax.swing.JLabel();
        lblbarrio = new javax.swing.JLabel();
        lblBarrio = new javax.swing.JLabel();
        lbldireccion = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        btnModificar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelGestionEmpleados.setBackground(new java.awt.Color(255, 153, 153));

        btnAtras.setBackground(new java.awt.Color(204, 204, 204));
        btnAtras.setText("Atras");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        lblTitulo.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        lblTitulo.setText("Administrar empleados");

        btnAgregarEmpleado.setBackground(new java.awt.Color(51, 0, 0));
        btnAgregarEmpleado.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        btnAgregarEmpleado.setForeground(new java.awt.Color(255, 255, 255));
        btnAgregarEmpleado.setText("Agregar empleado");
        btnAgregarEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarEmpleadoActionPerformed(evt);
            }
        });

        btnBuscar.setBackground(new java.awt.Color(51, 0, 0));
        btnBuscar.setForeground(new java.awt.Color(255, 255, 255));
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnEliminarEmpleado.setBackground(new java.awt.Color(51, 0, 0));
        btnEliminarEmpleado.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        btnEliminarEmpleado.setForeground(new java.awt.Color(255, 255, 255));
        btnEliminarEmpleado.setText("Eliminar empleado");
        btnEliminarEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarEmpleadoActionPerformed(evt);
            }
        });

        txtBuscarEmpleado.setText("Ingrese numero de legajo");

        lblapellido.setText("Apellido:");

        lblnombre.setText("Nombre:");

        lbldni.setText("DNI:");

        lblcorreo.setText("Correo:");

        lbltelefono.setText("Telefono:");

        lblareaTrabajo.setText("Area de trabajo:");

        lbldomicilio.setText("DOMICILIO:");

        lbllocalidad.setText("Localidad:");

        lblbarrio.setText("Barrio:");

        lbldireccion.setText("Direccion:");

        btnModificar.setBackground(new java.awt.Color(51, 0, 0));
        btnModificar.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(255, 255, 255));
        btnModificar.setText("Modificar Empleado");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelGestionEmpleadosLayout = new javax.swing.GroupLayout(panelGestionEmpleados);
        panelGestionEmpleados.setLayout(panelGestionEmpleadosLayout);
        panelGestionEmpleadosLayout.setHorizontalGroup(
            panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnAtras))
                            .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                        .addComponent(lbltelefono)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                        .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblapellido)
                                            .addComponent(lbldni))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(lblApellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblDni, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                        .addComponent(lblnombre)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                        .addComponent(lblcorreo)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblCorreo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                        .addComponent(lblareaTrabajo)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblArea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                        .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                .addGap(187, 187, 187)
                                .addComponent(lblTitulo))
                            .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtBuscarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnAgregarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEliminarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnModificar)))
                        .addGap(0, 75, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelGestionEmpleadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbldomicilio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                        .addComponent(lbldireccion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                        .addComponent(lbllocalidad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                        .addComponent(lblbarrio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblBarrio, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelGestionEmpleadosLayout.setVerticalGroup(
            panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelGestionEmpleadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(btnAgregarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGestionEmpleadosLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblapellido, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelGestionEmpleadosLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbldni, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblDni, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblcorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbltelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblareaTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblArea, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbldomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbllocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblbarrio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBarrio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelGestionEmpleadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbldireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(btnAtras)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGestionEmpleados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGestionEmpleados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        dispose();
        VentanaPrincipal ventanaPrincipal = new VentanaPrincipal(inmobiliaria);
        ventanaPrincipal.setVisible(true);
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        String legajo = txtBuscarEmpleado.getText().trim().toUpperCase();
        Integer numeroLegajo = Integer.valueOf(legajo);
        LibroEmpleado registroEmpleados = inmobiliaria.getRegistroEmpleados();
        try{
            Empleado empleadoBuscado = registroEmpleados.buscarEmpleadoEnRegistro(numeroLegajo);
            Ubicacion domicilioEmpleado = empleadoBuscado.getDomicilioActual();
            lblApellido.setText(empleadoBuscado.getApellidoDeLaPersona());
            lblNombre.setText(empleadoBuscado.getNombreDeLaPersona());
            lblDni.setText(empleadoBuscado.getDNIdeLaPersona());
            lblCorreo.setText(empleadoBuscado.getCorreoDeLaPersona());
            lblTelefono.setText(empleadoBuscado.getTelefonoDeLaPersona());
            lblArea.setText(empleadoBuscado.getAreaDeTrabajoDelEmpleado());
            lblLocalidad.setText(domicilioEmpleado.getLocalidad());
            lblBarrio.setText(domicilioEmpleado.getBarrio());
            lblDireccion.setText(domicilioEmpleado.getDireccion());
        }catch (ClienteInexistenteEnRegistroException e){
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(rootPane, "Error, el empleado no fue agregado al registro", "Alerta",0);
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnAgregarEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarEmpleadoActionPerformed
        dispose();
        VentanaAgregarEmpleado ventanaAgregarEmpleado = new VentanaAgregarEmpleado(inmobiliaria);
        ventanaAgregarEmpleado.setVisible(true);
    }//GEN-LAST:event_btnAgregarEmpleadoActionPerformed

    private void btnEliminarEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarEmpleadoActionPerformed
        String legajo = txtBuscarEmpleado.getText().trim().toUpperCase();
        Integer numeroLegajo = Integer.valueOf(legajo);
        LibroEmpleado registroEmpleados = inmobiliaria.getRegistroEmpleados();
        try{
            Empleado empleadoBuscado = registroEmpleados.buscarEmpleadoEnRegistro(numeroLegajo);
            registroEmpleados.eliminarEmpleadoDelRegistro(numeroLegajo);
            JOptionPane.showMessageDialog(rootPane, "Empleado eliminado OK");
            lblApellido.setText("");
            lblNombre.setText("");
            lblDni.setText("");
            lblCorreo.setText("");
            lblTelefono.setText("");
            lblArea.setText("");
            lblLocalidad.setText("");
            lblBarrio.setText("");
            lblDireccion.setText("");
            txtBuscarEmpleado.setText("Ingrese un numero de legajo");
        }catch (ClienteInexistenteEnRegistroException e){
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(rootPane, "Error, el empleado no fue agregado al registro", "Alerta",0);
        }
        
    }//GEN-LAST:event_btnEliminarEmpleadoActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        String legajo = txtBuscarEmpleado.getText().trim().toUpperCase();
        Integer numeroLegajo = Integer.valueOf(legajo);
        LibroEmpleado registroEmpleados = inmobiliaria.getRegistroEmpleados();
        try{
            Empleado empleadoBuscado = registroEmpleados.buscarEmpleadoEnRegistro(numeroLegajo);
            VentanaModificarEmpleado ventanaModificar = new VentanaModificarEmpleado(empleadoBuscado, inmobiliaria);
            ventanaModificar.setVisible(true);
            dispose();
        } catch (ClienteInexistenteEnRegistroException e){
            System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(rootPane, "Error, el empleado no fue agregado al registro", "Alerta",0);
        }
    }//GEN-LAST:event_btnModificarActionPerformed

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaGestionEmpleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaGestionEmpleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaGestionEmpleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaGestionEmpleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new VentanaGestionEmpleados().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarEmpleado;
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEliminarEmpleado;
    private javax.swing.JButton btnModificar;
    private javax.swing.JLabel lblApellido;
    private javax.swing.JLabel lblArea;
    private javax.swing.JLabel lblBarrio;
    private javax.swing.JLabel lblCorreo;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblDni;
    private javax.swing.JLabel lblLocalidad;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblapellido;
    private javax.swing.JLabel lblareaTrabajo;
    private javax.swing.JLabel lblbarrio;
    private javax.swing.JLabel lblcorreo;
    private javax.swing.JLabel lbldireccion;
    private javax.swing.JLabel lbldni;
    private javax.swing.JLabel lbldomicilio;
    private javax.swing.JLabel lbllocalidad;
    private javax.swing.JLabel lblnombre;
    private javax.swing.JLabel lbltelefono;
    private javax.swing.JPanel panelGestionEmpleados;
    private javax.swing.JTextField txtBuscarEmpleado;
    // End of variables declaration//GEN-END:variables
}
