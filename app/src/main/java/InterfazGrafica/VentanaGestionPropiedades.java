
package InterfazGrafica;
import inmobiliaria.*;
public class VentanaGestionPropiedades extends javax.swing.JFrame {

    Inmobiliaria inmobiliaria;
    public VentanaGestionPropiedades(Inmobiliaria _inmobiliaria) {
        this.inmobiliaria = _inmobiliaria;
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelGestionPropiedades = new javax.swing.JPanel();
        lbltitulo = new javax.swing.JLabel();
        btnAgregarPropiedad = new javax.swing.JButton();
        btnAtras = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        panelGestionPropiedades.setBackground(new java.awt.Color(255, 153, 153));

        lbltitulo.setFont(new java.awt.Font("Verdana", 1, 36)); // NOI18N
        lbltitulo.setText("Administrar Propiedades");

        btnAgregarPropiedad.setBackground(new java.awt.Color(51, 0, 0));
        btnAgregarPropiedad.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        btnAgregarPropiedad.setForeground(new java.awt.Color(255, 255, 255));
        btnAgregarPropiedad.setText("Agregar propiedad");
        btnAgregarPropiedad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarPropiedadActionPerformed(evt);
            }
        });

        btnAtras.setBackground(new java.awt.Color(204, 204, 204));
        btnAtras.setText("Atras");
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        btnBuscar.setBackground(new java.awt.Color(51, 0, 0));
        btnBuscar.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        btnBuscar.setForeground(new java.awt.Color(255, 255, 255));
        btnBuscar.setText("Buscar propiedad");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelGestionPropiedadesLayout = new javax.swing.GroupLayout(panelGestionPropiedades);
        panelGestionPropiedades.setLayout(panelGestionPropiedadesLayout);
        panelGestionPropiedadesLayout.setHorizontalGroup(
            panelGestionPropiedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGestionPropiedadesLayout.createSequentialGroup()
                .addGroup(panelGestionPropiedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelGestionPropiedadesLayout.createSequentialGroup()
                        .addGroup(panelGestionPropiedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelGestionPropiedadesLayout.createSequentialGroup()
                                .addGap(156, 156, 156)
                                .addComponent(lbltitulo))
                            .addGroup(panelGestionPropiedadesLayout.createSequentialGroup()
                                .addGap(36, 36, 36)
                                .addComponent(btnAgregarPropiedad)
                                .addGap(104, 104, 104)
                                .addComponent(btnBuscar)))
                        .addGap(0, 170, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelGestionPropiedadesLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAtras)))
                .addContainerGap())
        );
        panelGestionPropiedadesLayout.setVerticalGroup(
            panelGestionPropiedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGestionPropiedadesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbltitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addGroup(panelGestionPropiedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarPropiedad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 364, Short.MAX_VALUE)
                .addComponent(btnAtras)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGestionPropiedades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelGestionPropiedades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        VentanaPrincipal ventanaPrincipal = new VentanaPrincipal(inmobiliaria);
        ventanaPrincipal.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnAgregarPropiedadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarPropiedadActionPerformed
        VentanaAgregarPropiedad ventanaAgregarPropiedad = new VentanaAgregarPropiedad(inmobiliaria);
        ventanaAgregarPropiedad.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnAgregarPropiedadActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        VentanaBuscarPropiedad buscarPropiedad = new VentanaBuscarPropiedad(inmobiliaria);
        buscarPropiedad.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnBuscarActionPerformed

    
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(VentanaGestionPropiedades.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(VentanaGestionPropiedades.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(VentanaGestionPropiedades.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(VentanaGestionPropiedades.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new VentanaGestionPropiedades().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarPropiedad;
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JLabel lbltitulo;
    private javax.swing.JPanel panelGestionPropiedades;
    // End of variables declaration//GEN-END:variables
}
