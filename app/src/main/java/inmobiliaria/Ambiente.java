package inmobiliaria;

public class Ambiente {
    private String nombreDelAmbiente;
    private Float metrosCuadrados; 

    //metodo constructor
    public Ambiente(String _nombreDelAmbiente, Float _metrosCuadrados){
        this.metrosCuadrados = _metrosCuadrados;
        this.nombreDelAmbiente = _nombreDelAmbiente;
    }

    public Ambiente() {
    }

    //metodos get
    public Float getMetrosCuadradosDelAmbiente(){
        return metrosCuadrados;
    }
    public String getNombreDelAmbiente(){
        return nombreDelAmbiente;
    }

    //metodos set
    public void setMetrosCuadradosDelAmbiente(Float nuevosMetrosCuadrados){ //metodo que modifica los metros cuadrados de un ambiente, por si en algun futuro se modifica la propiedad.
        this.metrosCuadrados = nuevosMetrosCuadrados;
    }
    public void setNombreDelAmbiente(String nuevoNombre){
        this.nombreDelAmbiente = nuevoNombre;
    }

}
