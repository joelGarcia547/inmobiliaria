package inmobiliaria;

import InterfazGrafica.*;

public class App {
    public String getGreeting() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        Ubicacion ubicacionInmobiliaria = new Ubicacion("San Fernando del Valle de Catamarca", " ", "Av Belgrano 787");
        Inmobiliaria inmobiliaria = new Inmobiliaria("Mi Kasa", ubicacionInmobiliaria, "inmobiliariaMikasa@gmail.com", "+54 3834 123456");
        VentanaPrincipal ventanaPrincipal = new VentanaPrincipal(inmobiliaria);
        ventanaPrincipal.setVisible(true);
        System.out.println(new App().getGreeting());
    }
}
