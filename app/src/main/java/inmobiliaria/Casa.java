package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

import Exceptions.AmbienteInexistenteEnListaException;

import Exceptions.ListaDeAmbientesLlenaException;

public class Casa extends Propiedad{
    private Integer cantidadDeAmbientes;
    private List<Ambiente> listaDeAmbientesDeLaCasa;
   
    public Casa(Integer _cantidadDeAmbientes, Ubicacion _ubicacionDeLaPropiedad ,Float _metrosCuadradosDeLapropiedad, String _estadoDeLaPropiedad, Float precioPropiedad){
        super(_ubicacionDeLaPropiedad, _metrosCuadradosDeLapropiedad, _estadoDeLaPropiedad, precioPropiedad);
        this.cantidadDeAmbientes = _cantidadDeAmbientes;
        listaDeAmbientesDeLaCasa = new ArrayList<Ambiente>();

    }

    public Integer getCantidadDeAmbientes(){
        return cantidadDeAmbientes;
    }

    public void setCantidadDeAmbientes(Integer _nuevaCantidad){
        this.cantidadDeAmbientes = _nuevaCantidad;
    }

    public List<Ambiente> getListaDeAmbientes(){
        return listaDeAmbientesDeLaCasa;
    }

    //otros metodos
    public void agregarAmbienteALaListaDeLaCasa(Ambiente _ambienteParaAgregar) throws ListaDeAmbientesLlenaException{

            if(listaDeAmbientesDeLaCasa.size() < getCantidadDeAmbientes()){
                listaDeAmbientesDeLaCasa.add(_ambienteParaAgregar);
            }else{
                throw new ListaDeAmbientesLlenaException("Lista de ambientes completada");
            }
    }

    public Ambiente buscarAmbienteEnLista(Ambiente _ambienteABuscar) throws AmbienteInexistenteEnListaException{
        Ambiente _ambienteEncontrado = null;
        for(Ambiente _buscar : listaDeAmbientesDeLaCasa){
            if(_buscar.equals(_ambienteABuscar)){
                _ambienteEncontrado = _buscar;
            }
        }

        if(_ambienteEncontrado == (null)){
            throw new AmbienteInexistenteEnListaException("El ambiente no fue agregado a la propiedad");
        }
        return _ambienteEncontrado;
    }

    public Boolean verificarAmbiente(Ambiente _ambienteAVerificar){
        Boolean existeAmbiente = false;
        for (Ambiente ambiente : listaDeAmbientesDeLaCasa) {
            if(ambiente.equals(_ambienteAVerificar)){
                existeAmbiente = true;
            }
        }
        return existeAmbiente;
    }

    public void eliminarAmbiente(Ambiente _ambienteAEliminar)throws AmbienteInexistenteEnListaException{
        Boolean ambienteEncontrado = false;
        ambienteEncontrado = verificarAmbiente(_ambienteAEliminar);
        if(ambienteEncontrado.equals(true)){
            listaDeAmbientesDeLaCasa.remove(_ambienteAEliminar);
            setCantidadDeAmbientes(listaDeAmbientesDeLaCasa.size());
        }else{
            throw new AmbienteInexistenteEnListaException("El ambiente no fue agregado a la propiedad");
        }
    }

    //no borrar, metodo que se va usar en la GUI. joa
    /*public void modificarAmbienteDeLaLista(Ambiente ambienteAModificar) throws AmbienteInexistenteEnListaException{
        Boolean ambienteEncontrado = false;
        ambienteEncontrado = verificarAmbiente(ambienteAModificar);
        if(ambienteEncontrado.equals(true)){
            ambienteAModificar.setMetrosCuadradosDelAmbiente(//lo que nos ingrese el usuario);
            ambienteAModificar.setNombreDelAmbiente(//lo que nos ingrese el usuario);
        }else{
            throw new AmbienteInexistenteEnListaException("El ambiente no fue agregado a la propiedad");
        }
    }*/
}














