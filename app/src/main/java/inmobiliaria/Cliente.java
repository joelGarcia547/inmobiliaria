package inmobiliaria;

public class Cliente extends Persona{
    private Integer numeroDeOperacion;

    //Constructor de "Cliente"
    public Cliente(String _nombre, String _apellido, String _dniDelCliente, String _correoDelCliente,
                   String _telefonoDelCliente, Ubicacion _domicilioActual, Integer numeroDeOperacion){
        super(_nombre, _apellido, _dniDelCliente, _correoDelCliente, _telefonoDelCliente, _domicilioActual);
        this.numeroDeOperacion = numeroDeOperacion;
    }

    //Metodos de "Cliente"
    public String toString(){
        return "datos del cliente";
    }

    public Integer getNumeroDeOperacion(){
        return numeroDeOperacion;
    }

    /*public void historialFinanciero() {
        System.out.println("Historial financiero del Cliente");
    } */
    
    //sobreescribo Metodo Equals
    public boolean equals (Object o){
        Cliente clienteAComparar = (Cliente) o;

        return this.getDNIdeLaPersona().equals(clienteAComparar.getDNIdeLaPersona());
    }
}
