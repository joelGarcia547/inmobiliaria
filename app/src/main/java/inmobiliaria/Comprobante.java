package inmobiliaria;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Comprobante {
    private Integer numeroDeFactura;
    private Propiedad propiedad;
    private Float precioDeLaOperacion;
    private Boolean comprobantePagado; //Pagado(true) o NoPagado(false)
    private LocalDate fechaDeLaOperacionActual;
    private Cliente cliente;

    public Comprobante(Propiedad propiedad, Float precioDeLaOperacion, Boolean estaPagado,
    LocalDate fechaDeLaOperacionActual, Cliente cliente, Integer numeroDeFactura){
        this.propiedad = propiedad;
        this.precioDeLaOperacion = precioDeLaOperacion;
        this.comprobantePagado = estaPagado;
        this.fechaDeLaOperacionActual = fechaDeLaOperacionActual;
        this.cliente = cliente;
        this.numeroDeFactura = numeroDeFactura;
    }

    public Comprobante(){}

    private List<Facturable> _factura = new ArrayList<Facturable>();

    public void setPropiedadAFacturar(Propiedad _propiedad){
        this.propiedad = _propiedad;
    }
    public Propiedad getPropiedadAFacturar(){
        return propiedad;
    }

    public void setPrecioDeLaOperacion(Float _precio){
        this.precioDeLaOperacion = _precio;
    }
    public Float getPrecioDeLaOperacion(){
        return precioDeLaOperacion;
    }

    public void setEstadoDeLaOperacion(Boolean _estadoDeOperacion){
        this.comprobantePagado = _estadoDeOperacion; 
    }
    public Boolean getEstadoDeLaOperacion(){
        return comprobantePagado;
    }

    public void setCodigoDeOperacion(Integer _codigoNuevo){
        this.numeroDeFactura = _codigoNuevo;
    }
    public Integer getCodigoDeOperacion(){
        return numeroDeFactura;
    }

    public void setFechaDeLaOperacionActual(LocalDate _diaDeOperacionRealizada){
        this.fechaDeLaOperacionActual = _diaDeOperacionRealizada;
    }
    public LocalDate getFechaDeLaOperacionActual(){
        return fechaDeLaOperacionActual;
    }

    public void setClienteDeComprobante(Cliente _clienteOperacion){
        this.cliente = _clienteOperacion;
    }
    public Cliente getClienteDeComprobante(){
        return cliente;
    }

    public String toString(){
        return "Su pago fue efectuado correctamente";
    }

    public void agregarNuevaPropiedadAFacturar(Propiedad _propiedadAFacturar, Integer _cantDePropiedades){
        _factura.add(new Facturable(_propiedadAFacturar, _cantDePropiedades));
    }

    public Float calcularPrecioTotal(){
        Float total = 0f;
        for(Facturable _lista : _factura){
            total = (total + _lista.calcularSubTotal());
        }

        return total;
    }

    public List<Facturable> getPropiedadesAFacturar(){
        return _factura;
    }

    //CLASE INTERNA FACTURABLE
    private class Facturable{
        private Propiedad propiedad;  //casa, departamento o local
        private Integer _cantidadDePropiedades;

        public Facturable(Propiedad propiedad, Integer _cantidadDePropiedades){
            this.propiedad = propiedad;
            this._cantidadDePropiedades = _cantidadDePropiedades;
        }

        public Float calcularSubTotal(){
            Float subTotal = 0f;
            subTotal = (_cantidadDePropiedades * propiedad.getPrecioDePropiedad());
            return subTotal;
        }
    }
}
