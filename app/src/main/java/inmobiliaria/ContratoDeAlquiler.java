package inmobiliaria;

import java.time.LocalDate;
import java.time.Period;

public class ContratoDeAlquiler {
    private Alquiler alquiler;

    public ContratoDeAlquiler(Propiedad _propiedad, Boolean estaPagado, LocalDate fechaInicio,
                              LocalDate fechaFinal, LocalDate fechaOperacionAlquiler,
                              Float precioPactado, Cliente cliente, String formaDeEstadia){
        alquiler = new Alquiler(_propiedad, estaPagado, fechaInicio, fechaFinal, fechaOperacionAlquiler, precioPactado, cliente, formaDeEstadia);
    }

    public Alquiler getContratoDeAlquiler(){
        return alquiler;
    }

    //CLASE INTERNA
    public class Alquiler {
        private Propiedad propiedadAAlquilar;
        private Boolean estaPagado; //el alquiler esta pagado?
        private LocalDate fechaDeOperacionDeAlquiler;
        private LocalDate fechaInicio;
        private LocalDate fechaFinaliza;
        private Float precioPactadoPorDia;
        private Cliente cliente;
        private String formaDeEstadia; //por dia o por mes
        private Comprobante facturaDeAlquiler;
        Integer i = 0000;

        public Alquiler(Propiedad _propiedadAlquilar, Boolean _estaPagado ,LocalDate _fechaInicio,
                        LocalDate _fechaFinaliza, LocalDate _fechaOperacionDeAlquiler ,
                        Float _precioPactado, Cliente _clienteAlquilar, String _formaDeEstadia){
            this.propiedadAAlquilar = _propiedadAlquilar;
            this.estaPagado = _estaPagado;
            this.fechaInicio = _fechaInicio;
            this.fechaFinaliza = _fechaFinaliza;
            this.fechaDeOperacionDeAlquiler = _fechaOperacionDeAlquiler;
            this.precioPactadoPorDia = _precioPactado; 
            this.cliente = _clienteAlquilar;
            this.formaDeEstadia = _formaDeEstadia;
            if((estaPagado == true) && (propiedadAAlquilar.getEstadoDeLaPropiedad().equalsIgnoreCase("disponible"))){ //si la propiedad esta disponible, entonces genera el comprobante
                facturaDeAlquiler = new Comprobante(propiedadAAlquilar, precioPactadoPorDia, estaPagado, fechaDeOperacionDeAlquiler, cliente, i);
                propiedadAAlquilar.setEstadoDeLaPropiedad("alquilada");
                i++;
            }
        }

        //METODOS ACCESORES DE DATOS GET
        public Propiedad getPropiedadAAlquilar(){
            return propiedadAAlquilar;
        }

        public Boolean getEstaPagadoElAlquiler(){
            return estaPagado;
        }

        public LocalDate getFechaDeInicioDeContratoDeAlquiler(){
            return fechaInicio;
        }

        public LocalDate getFechaDeFinalizacionDeContratoDeAlquiler(){
            return fechaFinaliza;
        }

        public LocalDate getFechaDeOperacionDeAlquiler(){
            return fechaDeOperacionDeAlquiler;
        }
      
        public Float getPrecioPactadoPorDia(){
            return precioPactadoPorDia;
        }

        public Cliente getClienteDeAquiler(){
            return cliente;
        }

        public String getFormaDeEstadia(){
            return formaDeEstadia;
        }

        public Comprobante getFacturaDeAlquiler(){
            return facturaDeAlquiler;
        }

        public Integer determinarEstadia(){
            Integer estadia = 0;
            if(formaDeEstadia.equalsIgnoreCase("Mensual")){
                estadia = calcularCantidadDeMeses();
            } else {
                estadia = calcularCantidadDeDias();
            }
            return estadia;
        }

        private Integer calcularCantidadDeMeses(){
            Period periodo = Period.between(fechaInicio, fechaFinaliza);
            return periodo.getMonths();
        }

        private Integer calcularCantidadDeDias(){
            Period periodo = Period.between(fechaInicio, fechaFinaliza);
            return periodo.getDays();
        }

        public Float calcularPrecioTotalEstadia(Boolean _toallaYSabana, Float _precioOpcional){
            Float precioFinal = 0f;
            Integer estadia = 0;
            if(formaDeEstadia.equalsIgnoreCase("Mensual")){
                estadia = determinarEstadia();  //en MESES
                precioFinal = (30 * precioPactadoPorDia) * estadia;
            } else {
                if(_toallaYSabana == true){
                    estadia = determinarEstadia(); //en DIAS
                    precioFinal = (precioPactadoPorDia * estadia) + _precioOpcional;
                } else {
                    estadia = determinarEstadia(); //en DIAS
                    precioFinal = precioPactadoPorDia * estadia;
                }
 
            }

            return precioFinal;
        }
    }
}
