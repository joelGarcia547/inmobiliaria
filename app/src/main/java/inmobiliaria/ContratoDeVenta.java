package inmobiliaria;

import java.time.LocalDate;

public class ContratoDeVenta {
    private Venta venta;
    
    public ContratoDeVenta(Propiedad propiedadEnVenta, Float precioPactadoDeVenta,
                          Boolean estaEnVenta, Cliente clienteDeVenta, LocalDate fechaDeOperacionRealizada){
        venta = new Venta(propiedadEnVenta, precioPactadoDeVenta, estaEnVenta, clienteDeVenta, fechaDeOperacionRealizada);
    }

    public Venta getComprobanteDeVenta(){
        return venta;
    }

    public class Venta{
        private Propiedad propiedadEnVenta;
        private Float unPrecioPactado;
        private Boolean estaPagado; //para saber si esta en venta o no, es decir si esta "disponible"
        private Cliente cliente;
        private LocalDate fechaDeVenta;
        private Comprobante facturaDeVenta; 
        Integer i = 0000; //codigo de id de comprobante

        public Venta(Propiedad _propiedad, Float _precioVenta, Boolean _estaPagado ,
                    Cliente _cliente, LocalDate _fechaDeOperacionRealizada){
            this.propiedadEnVenta = _propiedad;
            this.unPrecioPactado = _precioVenta;
            this.estaPagado = _estaPagado;
            this.cliente = _cliente;
            this.fechaDeVenta = _fechaDeOperacionRealizada;
            if((estaPagado == true) && (propiedadEnVenta.getEstadoDeLaPropiedad().equalsIgnoreCase("disponible"))){ //si la propiedad esta disponible, entonces genera el comprobante
                facturaDeVenta = new Comprobante(propiedadEnVenta, unPrecioPactado, estaPagado, fechaDeVenta, cliente, i);
                propiedadEnVenta.setEstadoDeLaPropiedad("vendida");
                i++;
            } 
        }

        public Propiedad getPropiedadEnVenta(){
            return propiedadEnVenta;
        }

        public Float getPrecioPactado(){
            return unPrecioPactado;
        }

        public Boolean getEstaPagadaLaVenta(){
            return estaPagado;
        }

        public Cliente getClienteDeVenta(){
            return cliente;
        }

        public LocalDate getFechaDeOperacionDeVentaRealizada(){
            return fechaDeVenta;
        }

        public Comprobante getFacturaDeVenta(){
            return facturaDeVenta;
        }
    }
}
