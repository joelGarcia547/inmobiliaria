package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

import Exceptions.AmbienteInexistenteEnListaException;

import Exceptions.ListaDeAmbientesLlenaException;

public class Departamento extends Propiedad{
    private Integer pisoDelDepartamento;
    private Integer numeroDelDepartamento;
    private Boolean cocheraDelDepartamento;         //verdadero si tiene cochera, falso si no la tiene
    private Integer cantidadDeAmbientes;
    private List<Ambiente> listaDeAmbientesDelDepartamento; 
    //metodos constructores
    public Departamento(Integer _pisoDelDepartamento,Integer _numeroDelDpto,Boolean _cocheraDelDepartamento, Integer _cantidadDeAmbientes,
                        Ubicacion _ubicacionDelDpto, Float _metrosCuadradosDelDpto, String _estadoDeLaPropiedad,Float _precioDeLaPropiedad){
        super(_ubicacionDelDpto, _metrosCuadradosDelDpto, _estadoDeLaPropiedad, _precioDeLaPropiedad);
        this.pisoDelDepartamento = _pisoDelDepartamento;
        this.numeroDelDepartamento = _numeroDelDpto;
        this.cocheraDelDepartamento = _cocheraDelDepartamento;
        this.cantidadDeAmbientes = _cantidadDeAmbientes;
        this.listaDeAmbientesDelDepartamento = new ArrayList<Ambiente>();
    }

    //metodos set
    public void setPisoDelDepartamento(Integer _pisoDelDepartamento){
        this.pisoDelDepartamento = _pisoDelDepartamento;
    }
    public void setCocheraDelDepartamento(Boolean _cocheraDelDepartamento){    //verdadero si tiene cochera, falso si no la tiene
        this.cocheraDelDepartamento = _cocheraDelDepartamento;
    }
    public void setCantidadDeAmbientes(Integer _cantidadDeAmbientes){
        this.cantidadDeAmbientes = _cantidadDeAmbientes;
    }

    //metodos get
    public Integer getPisoDelDepartamento(){
        return pisoDelDepartamento;
    }
    public Integer getNumeroDepartamento(){
        return numeroDelDepartamento;
    }
    public Boolean getCocheraDelDepartamento(){  //verdadero si tiene cochera, falso si no la tiene
        return cocheraDelDepartamento;
    }
    public Integer getCantidadDeAmbientes(){
        return cantidadDeAmbientes;
    }
    public List<Ambiente> getListaDeAmbientesDelDpto(){
        return listaDeAmbientesDelDepartamento;
    }

    //otros metodos
    public void agregarAmbienteAlDpto(Ambiente ambienteAAgregar) throws ListaDeAmbientesLlenaException{
        Integer cantidadDeAmbientesDeLaLista = listaDeAmbientesDelDepartamento.size();

            if(cantidadDeAmbientesDeLaLista < getCantidadDeAmbientes()){
                listaDeAmbientesDelDepartamento.add(ambienteAAgregar);
            }else{
                throw new ListaDeAmbientesLlenaException("Lista de ambientes completada");
            }
    }

    public Boolean verificarAmbiente(Ambiente ambienteAVerificar){
        Boolean ambienteEncontrado = false;
        for (Ambiente ambiente : listaDeAmbientesDelDepartamento) {
            if(ambiente.equals(ambienteAVerificar)){
                ambienteEncontrado = true;
            }
        }
        return ambienteEncontrado;
    }

    public void eliminarAmbienteDelDpto(Ambiente ambienteAEliminar) throws AmbienteInexistenteEnListaException{
        Boolean ambienteAgregado = false;
        ambienteAgregado = verificarAmbiente(ambienteAEliminar);

        if(ambienteAgregado.equals(true)){
            listaDeAmbientesDelDepartamento.remove(ambienteAEliminar);
            setCantidadDeAmbientes(listaDeAmbientesDelDepartamento.size());
        }else{
            throw new AmbienteInexistenteEnListaException("El ambiente no fue agregado a la lista de la propiedad");
        }
    }

    public Ambiente buscarAmbienteEnLaLista(Ambiente ambienteABuscar) throws AmbienteInexistenteEnListaException{
        
        Ambiente ambienteADevolver = null;
        for (Ambiente ambiente : listaDeAmbientesDelDepartamento) {
            if(ambiente.equals(ambienteABuscar)){
                ambienteADevolver = ambiente;
            }
        }
        if(ambienteADevolver ==(null)){
            throw new AmbienteInexistenteEnListaException("El ambiente no fue agregado a la lista de la propiedad");
        }
        return ambienteADevolver;
    }

    //no borrar, metodo que se va usar en la GUI. joa
    /*public void modificarAmbienteDeLaLista(Ambiente ambienteAModificar) throws AmbienteInexistenteEnListaException{
        Boolean ambienteEncontrado = false;
        ambienteEncontrado = verificarAmbiente(ambienteAModificar);
        if(ambienteEncontrado.equals(true)){
            ambienteAModificar.setMetrosCuadradosDelAmbiente(//lo que nos ingrese el usuario);
            ambienteAModificar.setNombreDelAmbiente(//lo que nos ingrese el usuario);
        }else{
            throw new AmbienteInexistenteEnListaException("El ambiente no fue agregado a la propiedad");
        }
    }*/
}










