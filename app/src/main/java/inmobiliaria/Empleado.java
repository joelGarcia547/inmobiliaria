package inmobiliaria;

public class Empleado extends Persona{
    private Integer numeroDeLegajo = 0; //para identificar cada empleado, por ej un numero hexadecimal
    private String areaDeTrabajoDelEmpleado;

    public Empleado(String _nombre, String _apellido, String _dni,
    String _correoDeContacto, String _telefonoDeContacto, Ubicacion _domicilioActual,
    String areaDeTrabajoDelEmpleado){   
        
        super(_nombre, _apellido, _dni, _correoDeContacto, _telefonoDeContacto, _domicilioActual);
        this.areaDeTrabajoDelEmpleado = areaDeTrabajoDelEmpleado;  
    }
    
    public String toString(){
        return "datos del empleado";
    }

    public void setNumeroDeLegajo(Integer contador){
        this.numeroDeLegajo = contador;
    }
    public Integer getNumeroDeLegajo(){
        return numeroDeLegajo;
    }

    public void setAreaDeTrabajo(String _areaDeTrabajo){
        this.areaDeTrabajoDelEmpleado = _areaDeTrabajo;
    }
    public String getAreaDeTrabajoDelEmpleado(){
        return areaDeTrabajoDelEmpleado;
    }

    public void historialFinanciero() {
        System.out.println("historial financiero del empleado");
    }

}
