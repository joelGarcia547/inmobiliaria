package inmobiliaria;

public class Inmobiliaria {   //¿Seria la clase donde se instancia todo?
    private String nombreDeInmobiliaria;
    private Ubicacion ubicacionDeLaInmobiliaria;
    private String direccionDeCorreoElectronicoDeLaInmobiliaria;
    private String telefonoDeLaInmobiliaria;
    private LibroPropiedad _registroDePropiedades;
    private LibroCliente _registroDeClientes;
    private LibroEmpleado _registroEmpleado;

    //para patron singleton
    private static Inmobiliaria inmobiliaria;

    //metodos constructores
    public Inmobiliaria(String _nombreDeInmobiliaria, Ubicacion _ubicacionDEInmobiliaria, 
    String _correoDeInmobiliaria, String _telefonoDeInmobiliaria){
        this.nombreDeInmobiliaria = _nombreDeInmobiliaria;
        this.ubicacionDeLaInmobiliaria = _ubicacionDEInmobiliaria;
        this.direccionDeCorreoElectronicoDeLaInmobiliaria = _correoDeInmobiliaria;
        this.telefonoDeLaInmobiliaria = _telefonoDeInmobiliaria;
    }

    public Inmobiliaria(){}

    //patron singleton .joel
    public static Inmobiliaria instanciaInmobiliaria(String _nombreDeInmobiliaria, Ubicacion _ubicacionDEInmobiliaria, 
    String _correoDeInmobiliaria, String _telefonoDeInmobiliaria){
        if(inmobiliaria == null){
            inmobiliaria = new Inmobiliaria(_nombreDeInmobiliaria, _ubicacionDEInmobiliaria, _correoDeInmobiliaria, _telefonoDeInmobiliaria);
        }
        return inmobiliaria;
    }
    
    public void setLibroEmpleados(LibroEmpleado _libroEmpleado){
        _registroEmpleado = _libroEmpleado;
    }
    public void setLibroPropiedades(LibroPropiedad _libroPropiedades){
        _registroDePropiedades = _libroPropiedades;
    }

    public String getNombreDeInmobiliaria(){
        return nombreDeInmobiliaria;
    }
    public Ubicacion getUbicaionDeInmobiliaria(){
        return ubicacionDeLaInmobiliaria;
    }
    public String getCorreoDeInmobiliaria(){
        return direccionDeCorreoElectronicoDeLaInmobiliaria;
    }
    public String getTelefonoDeInmobiliaria(){
        return telefonoDeLaInmobiliaria;
    }
    public LibroPropiedad getRegistroDePropiedades(){
        return _registroDePropiedades;
    }
    public LibroCliente getRegistroDeClientes(){
        return _registroDeClientes;
    }
    public LibroEmpleado getRegistroEmpleados(){
        return _registroEmpleado;
    }

    
}















