package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

import Exceptions.ClienteExistenteEnRegistroException;
import Exceptions.ClienteInexistenteEnRegistroException;

public class LibroCliente{ 
    private List<Cliente> registroDeClientes;

    public LibroCliente(){
        this.registroDeClientes = new ArrayList<Cliente>();
    }

    public void agregarClienteARegistro(Cliente _clienteAResgistrar) throws ClienteExistenteEnRegistroException{
        Boolean existeCliente = verificarCliente(_clienteAResgistrar);
        if(existeCliente.equals(true)){
            throw new ClienteExistenteEnRegistroException("El cliente ya fue agregado anteriormente");
        }else{
            registroDeClientes.add(_clienteAResgistrar);
        }
    }

    public Boolean verificarCliente(Cliente clienteAVerificar){
        Boolean existe = false;
        for (Cliente cliente : registroDeClientes) {
            if(cliente.equals(clienteAVerificar)){
                existe = true;
            }
        }
        return existe;
    }

    public Cliente buscarClienteEnRegistro(String _dniAbuscar) throws ClienteInexistenteEnRegistroException{
        Cliente _clienteARetornar = null;
        String dniABuscar = _dniAbuscar.toUpperCase();
        for(Cliente cliente : registroDeClientes){
            if(cliente.getDNIdeLaPersona().equals(dniABuscar)){
                _clienteARetornar = cliente;
            }
        }

        if(_clienteARetornar == null){
            throw new ClienteInexistenteEnRegistroException("El cliente no fue agregado al registro");
        }
        
        return _clienteARetornar;
    }

    public void removerClienteDeRegistro(String _dniAEliminar) throws ClienteInexistenteEnRegistroException{
        String dniAEliminar = _dniAEliminar.toUpperCase();
        Cliente _clienteABorrar = null;

        try {
            _clienteABorrar = buscarClienteEnRegistro(dniAEliminar);
        } catch (ClienteInexistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }
        if(_clienteABorrar != null){
            registroDeClientes.remove(_clienteABorrar);
        }else{
            throw new ClienteInexistenteEnRegistroException("El cliente no fue agregado al registro");
        }
    }

    public List<Cliente> getRegistroDeClientes(){
        return registroDeClientes;
    }

    /*public void modificarCliente(Cliente clienteAModificar) throws ClienteInexistenteEnRegistroException{
        Boolean clienteExiste = false;
        clienteExiste = verificarCliente(clienteAModificar);
        if(clienteExiste.equals(true)){
            clienteAModificar.setApellidoPersona(//lo que recoja del txtField);
            clienteAModificar.setNombrePersona(//lo que recoja del txtField);
            clienteAModificar.setCorreoDeLaPersona(//lo que recoja del txtField);
            clienteAModificar.setTelefonoDeLaPersona(//lo que recoja del txtField);
            clienteAModificar.setUbicacionDeLaPersona(//lo que recoja del txtField);
        } else {
            throw new ClienteInexistenteEnRegistroException("El cliente no fue agregado al registro");
        }
    }*/
}
