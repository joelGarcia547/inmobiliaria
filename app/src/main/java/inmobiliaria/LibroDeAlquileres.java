package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

import Exceptions.ContratoDeAlquilerInexistenteException;
import Exceptions.ContratoDeAlquilerYaExistenteException;

public class LibroDeAlquileres {
    private List<ContratoDeAlquiler> registroDeAlquiler;

    public LibroDeAlquileres(){
        registroDeAlquiler = new ArrayList<ContratoDeAlquiler>();
    }

    public List<ContratoDeAlquiler> getRegistroDeAlquileres(){
        return registroDeAlquiler;
    }

    public void agregarContratoDeAlquilerAlRegistro(ContratoDeAlquiler _contratoAlquiler) throws ContratoDeAlquilerYaExistenteException{
        Boolean yaExisteEnRegistro = false;
        yaExisteEnRegistro = verificarExistenciaDeContratoDeAlquiler(_contratoAlquiler.getContratoDeAlquiler().getFacturaDeAlquiler().getCodigoDeOperacion());
        if(yaExisteEnRegistro == true){
            throw new ContratoDeAlquilerYaExistenteException("El contrato de Alquiler ya existe en el registro");
        } else {
            registroDeAlquiler.add(_contratoAlquiler);
        }
        
    }

    public Boolean verificarExistenciaDeContratoDeAlquiler(Integer _codigoDeFactura){
        Boolean siExiste = false;
        for(ContratoDeAlquiler _existe : registroDeAlquiler){
            if(_existe.getContratoDeAlquiler().getFacturaDeAlquiler().getCodigoDeOperacion().equals(_codigoDeFactura)){
                siExiste = true;
            }
        }

        return siExiste;
    }

    public ContratoDeAlquiler buscarContratoDeAlquilerEnLibro(Integer _codigoDeComprobante) throws ContratoDeAlquilerInexistenteException{
        ContratoDeAlquiler contratoEncontrado = null;
        for(ContratoDeAlquiler _encontrado : registroDeAlquiler){
            if(_encontrado.getContratoDeAlquiler().getFacturaDeAlquiler().getCodigoDeOperacion().equals(_codigoDeComprobante)){
                contratoEncontrado = _encontrado;
            }
        }

        if(contratoEncontrado == null){
            throw new ContratoDeAlquilerInexistenteException("El contrato de Alquiler no existe en el registro");
        }

        return contratoEncontrado;
    }

    public void removerContratoDeAlquilerDeRegistro(Integer _codigoDeFactura){
        try {
            ContratoDeAlquiler contratoABorrar = buscarContratoDeAlquilerEnLibro(_codigoDeFactura);
            registroDeAlquiler.remove(contratoABorrar);
        } catch (ContratoDeAlquilerInexistenteException e) {
            e.printStackTrace();
        }
    }

    public void modificarContratoDeAlquiler(ContratoDeAlquiler _contratoAModificar, ContratoDeAlquiler _contratoModificado){
        try {
            ContratoDeAlquiler aBuscarEnRegistro = buscarContratoDeAlquilerEnLibro(_contratoAModificar.getContratoDeAlquiler().getFacturaDeAlquiler().getCodigoDeOperacion());
            
            if(aBuscarEnRegistro != null){
                registroDeAlquiler.remove(aBuscarEnRegistro);
                registroDeAlquiler.add(_contratoModificado);
            }
        } catch (ContratoDeAlquilerInexistenteException e) {
            e.printStackTrace();
        }
    }

    public void listarContratoDeAlquiler(){
        for (ContratoDeAlquiler listaDeVenta : registroDeAlquiler) {
            System.out.println("Fecha De Operación (Venta): "+listaDeVenta.getContratoDeAlquiler().getFechaDeOperacionDeAlquiler());
            System.out.println("Codigo de Facturación: "+listaDeVenta.getContratoDeAlquiler().getFacturaDeAlquiler().getCodigoDeOperacion());
            System.out.println(">------------------- PROPIEDAD -------------------<");
            System.out.println("Localidad: "+listaDeVenta.getContratoDeAlquiler().getFacturaDeAlquiler().getPropiedadAFacturar().getUbicacionDePropiedad().getLocalidad());
            System.out.println("Barrio: "+listaDeVenta.getContratoDeAlquiler().getFacturaDeAlquiler().getPropiedadAFacturar().getUbicacionDePropiedad().getBarrio());
            System.out.println("Dirección: "+listaDeVenta.getContratoDeAlquiler().getFacturaDeAlquiler().getPropiedadAFacturar().getUbicacionDePropiedad().getDireccion());
            System.out.println("Precio De Propiedad: $"+listaDeVenta.getContratoDeAlquiler().getPropiedadAAlquilar().getPrecioDePropiedad());
            System.out.println("Estado De La Propiedad: "+listaDeVenta.getContratoDeAlquiler().getPropiedadAAlquilar().getEstadoDeLaPropiedad());
            System.out.println(">------------------- CLIENTE -------------------<");
            System.out.println(""+listaDeVenta.getContratoDeAlquiler().getClienteDeAquiler().getApellidoDeLaPersona()+", "+
                               listaDeVenta.getContratoDeAlquiler().getClienteDeAquiler().getNombreDeLaPersona());
            System.out.println("Teléfono: "+listaDeVenta.getContratoDeAlquiler().getClienteDeAquiler().getTelefonoDeLaPersona());
            System.out.println("Correo: "+listaDeVenta.getContratoDeAlquiler().getClienteDeAquiler().getCorreoDeLaPersona());
            System.out.println(">------------------- VENTA -------------------<");
            System.out.println("Total: "+listaDeVenta.getContratoDeAlquiler().getPrecioPactadoPorDia());
        }
    }

}
