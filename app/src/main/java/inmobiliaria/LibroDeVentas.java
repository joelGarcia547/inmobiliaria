package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

import Exceptions.ContratoDeVentaInexistenteException;
import Exceptions.ContratoDeVentaYaExistenteException;

public class LibroDeVentas {
    private List<ContratoDeVenta> registroDeVentas;

    public LibroDeVentas(){
        registroDeVentas = new ArrayList<ContratoDeVenta>();
    }

    public List<ContratoDeVenta> getRegistroDeVentas(){
        return registroDeVentas;
    }

    public void agregarContratoDeVentaARegistro(ContratoDeVenta _contratoVenta)throws ContratoDeVentaYaExistenteException{
        Boolean yaExisteEnRegistro = false;
        yaExisteEnRegistro = verificarExistenciaDeContratoDeVenta(_contratoVenta.getComprobanteDeVenta().getFacturaDeVenta().getCodigoDeOperacion());
        if(yaExisteEnRegistro == true){
            throw new ContratoDeVentaYaExistenteException("El contrato de venta ya existe en el registro");
        } else {
            registroDeVentas.add(_contratoVenta);
        }
    }

    public Boolean verificarExistenciaDeContratoDeVenta(Integer _codigoDeFactura){
        Boolean siExiste = false;
        for(ContratoDeVenta _existe : registroDeVentas){
            if(_existe.getComprobanteDeVenta().getFacturaDeVenta().getCodigoDeOperacion().equals(_codigoDeFactura)){
                siExiste = true;
            }
        }

        return siExiste;
    }

    public ContratoDeVenta buscarContratoDeVentaEnLibro(Integer _codigoDeComprobante) throws ContratoDeVentaInexistenteException{
        ContratoDeVenta contratoEncontrado = null;
        for(ContratoDeVenta _encontrado : registroDeVentas){
            if(_encontrado.getComprobanteDeVenta().getFacturaDeVenta().getCodigoDeOperacion().equals(_codigoDeComprobante)){
                contratoEncontrado = _encontrado;
            }
        }

        if(contratoEncontrado == null){
            throw new ContratoDeVentaInexistenteException("El contrato de venta no existe en el registro");
        }

        return contratoEncontrado;
    }

    public void removerContratoDeVentaDeRegistro(Integer _codigoDeFactura){
        try {
            ContratoDeVenta contratoABorrar = buscarContratoDeVentaEnLibro(_codigoDeFactura);
            registroDeVentas.remove(contratoABorrar);
        } catch (ContratoDeVentaInexistenteException e) {
            e.printStackTrace();
        }
        
    }
    
    public void modificarContratoDeVenta(ContratoDeVenta _contratoAModificar, ContratoDeVenta _contratoModificado){
        try {
            ContratoDeVenta aBuscarEnRegistro = buscarContratoDeVentaEnLibro(_contratoAModificar.getComprobanteDeVenta().getFacturaDeVenta().getCodigoDeOperacion());
            
            if(aBuscarEnRegistro != null){
                registroDeVentas.remove(aBuscarEnRegistro);
                registroDeVentas.add(_contratoModificado);
            }
        } catch (ContratoDeVentaInexistenteException e) {
            e.printStackTrace();
        }
    }

    public void listarContratoDeVenta(){
        for (ContratoDeVenta listaDeVenta : registroDeVentas) {
            System.out.println("Fecha De Operación (Venta): "+listaDeVenta.getComprobanteDeVenta().getFechaDeOperacionDeVentaRealizada());
            System.out.println("Codigo de Facturación: "+listaDeVenta.getComprobanteDeVenta().getFacturaDeVenta().getCodigoDeOperacion());
            System.out.println(">------------------- PROPIEDAD -------------------<");
            System.out.println("Localidad: "+listaDeVenta.getComprobanteDeVenta().getPropiedadEnVenta().getUbicacionDePropiedad().getLocalidad());
            System.out.println("Barrio: "+listaDeVenta.getComprobanteDeVenta().getFacturaDeVenta().getPropiedadAFacturar().getUbicacionDePropiedad().getBarrio());
            System.out.println("Dirección: "+listaDeVenta.getComprobanteDeVenta().getPropiedadEnVenta().getUbicacionDePropiedad().getDireccion());
            System.out.println("Precio De Propiedad: $"+listaDeVenta.getComprobanteDeVenta().getPropiedadEnVenta().getPrecioDePropiedad());
            System.out.println("Estado De La Propiedad: "+listaDeVenta.getComprobanteDeVenta().getPropiedadEnVenta().getEstadoDeLaPropiedad());
            System.out.println(">------------------- CLIENTE -------------------<");
            System.out.println(""+listaDeVenta.getComprobanteDeVenta().getClienteDeVenta().getApellidoDeLaPersona()+", "+
                               listaDeVenta.getComprobanteDeVenta().getClienteDeVenta().getNombreDeLaPersona());
            System.out.println("Teléfono: "+listaDeVenta.getComprobanteDeVenta().getClienteDeVenta().getTelefonoDeLaPersona());
            System.out.println("Correo: "+listaDeVenta.getComprobanteDeVenta().getClienteDeVenta().getCorreoDeLaPersona());
            System.out.println(">------------------- VENTA -------------------<");
            System.out.println("Total: "+listaDeVenta.getComprobanteDeVenta().getPrecioPactado());
        }
    }
}
