package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

import Exceptions.ClienteExistenteEnRegistroException;
import Exceptions.ClienteInexistenteEnRegistroException;

public class LibroEmpleado {
    private List<Empleado> registroEmpleados;
    private static Integer contador = 0;
    public LibroEmpleado(){
        this.registroEmpleados = new ArrayList<Empleado>();
    }

    public List<Empleado> getListaDeEmpleados(){
        return registroEmpleados;
    }
    
    public void agregarEmpleadoAlRegistro(Empleado empleadoParaAgregar)throws ClienteExistenteEnRegistroException{
        Boolean verificarEmpleado = false;
        verificarEmpleado = verificarEmpleadoEnReg(empleadoParaAgregar.getDNIdeLaPersona());
        if(verificarEmpleado.equals(true)){
            throw new ClienteExistenteEnRegistroException("El empleado ya fue registrado anteriormente");
        }else{
            registroEmpleados.add(empleadoParaAgregar);
            contador = contador +1;
            empleadoParaAgregar.setNumeroDeLegajo(contador);
        }
    }

    public Boolean verificarEmpleadoEnReg(String dniAComparar){
        Boolean existeEmpleado = false;
        for (Empleado empleado : registroEmpleados) {
            if(empleado.getDNIdeLaPersona().equals(dniAComparar)){
                existeEmpleado = true;
            }
        }
        return existeEmpleado;
    }

    public Empleado buscarEmpleadoEnRegistro(Integer _numeroDeLegajo)throws ClienteInexistenteEnRegistroException{
        Empleado empleadoBuscado = null;
        for (Empleado empleado : registroEmpleados) {
            if(empleado.getNumeroDeLegajo().equals(_numeroDeLegajo)){
                empleadoBuscado = empleado;
            }
        }
        if(empleadoBuscado == (null)){
            throw new ClienteInexistenteEnRegistroException("No fue posible encontrar al empleado");
        }
        return empleadoBuscado;
    }

    public void eliminarEmpleadoDelRegistro(Integer _numeroDeLegajoAEliminar) throws ClienteInexistenteEnRegistroException{
        Empleado empleadoQueNosLlega = null;

        try {
            empleadoQueNosLlega = buscarEmpleadoEnRegistro(_numeroDeLegajoAEliminar);
        } catch (ClienteInexistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        if(empleadoQueNosLlega != null){
            registroEmpleados.remove(empleadoQueNosLlega);
        }else{
            throw new ClienteInexistenteEnRegistroException("No fue posible encontrar al empleado");
        }
    }

    public void modificarEmpleado(Empleado empleadoAModificar, Empleado empleadoModificado) throws ClienteInexistenteEnRegistroException, ClienteExistenteEnRegistroException{
        Boolean existeEmpleado = false;
        existeEmpleado = verificarEmpleadoEnReg(empleadoAModificar.getDNIdeLaPersona());
        if(existeEmpleado.equals(true)){
            eliminarEmpleadoDelRegistro(empleadoAModificar.getNumeroDeLegajo());
            try{
                agregarEmpleadoAlRegistro(empleadoModificado);
            } catch(ClienteExistenteEnRegistroException e){
                System.out.println(e.getMessage());
            }
        } else{
            throw new ClienteInexistenteEnRegistroException("No fue posible encontrar al empleado");
        }
    }
}
