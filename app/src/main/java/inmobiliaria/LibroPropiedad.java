package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

import Exceptions.PropiedadRepetidaException;
import Exceptions.*;
public class LibroPropiedad {
    List<Propiedad> registroDePropiedades;

    public LibroPropiedad(){
        registroDePropiedades = new ArrayList<>();
    }

    public void agregarPropiedadALibro(Propiedad _propiedadAAgregar) throws PropiedadRepetidaException{
        Boolean existePropiedad = verificarPropiedad(_propiedadAAgregar);
        if(existePropiedad.equals(true)){
            throw new PropiedadRepetidaException("La propiedad ya fue agregada anteriormente");
        }else{
            registroDePropiedades.add(_propiedadAAgregar);
        }
    }

    public Boolean verificarPropiedad(Propiedad propiedadAVerificar){
        Boolean existe = false;
        for (Propiedad propiedad : registroDePropiedades) {
            if(propiedad.getUbicacionDePropiedad().equals(propiedadAVerificar.getUbicacionDePropiedad())){
                existe = true;
            }
        }
        return existe;
    }

    public Propiedad buscarPropiedadEnLibro(Ubicacion ubicacionPropiedad) throws PropiedadInexistenteException {
        Propiedad _propiedadEncontrada = null;

        for(Propiedad propiedad : registroDePropiedades){
            if(propiedad.getUbicacionDePropiedad().equals(ubicacionPropiedad)){
                _propiedadEncontrada = propiedad;
            }
        }

        if(_propiedadEncontrada == null){
            throw new PropiedadInexistenteException("La propiedad no fue agregada al registro");
        }
        return _propiedadEncontrada;
    }

    public void eliminarPropiedadDelLibro(Propiedad propiedadAEliminar) throws PropiedadInexistenteException{
        Boolean existe = false;
        existe = verificarPropiedad(propiedadAEliminar);
        if(existe.equals(true)){
            registroDePropiedades.remove(propiedadAEliminar);
        }else{
            throw new PropiedadInexistenteException("La propiedad no fue agregada al registro");
        }
    }

    public List<Propiedad> getRegistroDePropiedades(){
        return registroDePropiedades;
    }
    
}
