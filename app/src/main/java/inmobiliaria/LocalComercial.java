package inmobiliaria;

public class LocalComercial extends Propiedad{
    private Float metrosCuadradosDeVidriera;
    private Boolean poseeBanioElLocal;   //Si posee baño sera "true", de lo contrario sera "false".
    private Boolean poseeCocinaElLocal; //Si posee cocina sera "true", de lo contrario sera "false".

    public LocalComercial(Float _metrosCuadradosDeVidriera, Boolean _poseeBanio, Boolean _poseeCocina,
                          Ubicacion _ubicacionDelLocal, Float _metrosCuadradosDelLocal, String estadoDeLaPropiedad, Float _precioDelLocal){
        super(_ubicacionDelLocal, _metrosCuadradosDelLocal, estadoDeLaPropiedad, _precioDelLocal);
        this.metrosCuadradosDeVidriera = _metrosCuadradosDeVidriera;
        this.poseeBanioElLocal = _poseeBanio;
        this.poseeCocinaElLocal = _poseeCocina;

    }
    //metodos set
    public void setPoseeBanioElLocal(Boolean _poseeBanio){
        this.poseeBanioElLocal = _poseeBanio;
    }
    public void setPoseeCocinaElLocal(Boolean _poseeCocina){
        this.poseeCocinaElLocal = _poseeCocina;
    }
    public void setMetrosCuadradosDeVidriera(Float _metrosCuadradosVidriera){
        this.metrosCuadradosDeVidriera = _metrosCuadradosVidriera;
    }

    //metodos get
    public Float getMetrosCuadradosDeVidriera(){
        return metrosCuadradosDeVidriera;
    }
    public boolean getPoseeBanioElLocal(){
        return poseeBanioElLocal;
    }
    public boolean getPoseeCocinaElLocal(){
        return poseeCocinaElLocal;
    }

    //otros metodos.
}
