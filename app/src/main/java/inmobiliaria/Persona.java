package inmobiliaria;

public abstract class Persona { 
    private String nombre;
    private String apellido;
    private String dniDeLaPersona; 
    private String correoDeContacto;
    private String telefonoDeContacto;
    private Ubicacion domicilioActual;

    public Persona(String _nombre, String _apellido, String _dniDeLaPersona, String _correoDeContacto,
                   String _telefonoDeContacto, Ubicacion _domicilioActual){
        this.nombre = _nombre.toUpperCase();
        this.apellido = _apellido.toUpperCase();
        this.dniDeLaPersona = _dniDeLaPersona.toUpperCase();
        this.correoDeContacto = _correoDeContacto;
        this.telefonoDeContacto = _telefonoDeContacto;
        this.domicilioActual = _domicilioActual;
    }

    //lo dejo como comentario por el momento
    //public abstract void historialFinanciero();

    public void setNombrePersona(String _nombre){
        this.nombre = _nombre;
    }
    public void setApellidoPersona(String _apellido){
        this.apellido = _apellido;
    }
    public void setDniPersona(String _dni){
        this.dniDeLaPersona = _dni;
    }
    public void setCorreoDeLaPersona(String _correo){
        this.correoDeContacto = _correo;
    }
    public void setTelefonoDeLaPersona(String _telefono){
        this.telefonoDeContacto = _telefono;
    }
    public void setUbicacionDeLaPersona(Ubicacion _ubicacion){
        this.domicilioActual = _ubicacion;
    }

    public String getNombreDeLaPersona(){
        return nombre;
    }
    public String getApellidoDeLaPersona(){
        return apellido;
    }
    public String getDNIdeLaPersona(){
        return dniDeLaPersona;
    }
    public String getCorreoDeLaPersona(){
        return correoDeContacto;
    }
    public String getTelefonoDeLaPersona(){
        return telefonoDeContacto;
    }
    public Ubicacion getDomicilioActual(){
        return domicilioActual;
    }
}












