package inmobiliaria;

public class Propiedad {
    private Ubicacion ubicacionDePropiedad; //una propiedad tiene una ubicacion y una ubicacion una propiedad, independientemente de que esa prpiedad sean Locales, deptos o una casa.
    private String estadoDeLaPropiedad; //vendida, comprada, alquilada o disponible   atte.joel
    private Float metrosCuadradosDePropiedad;
    private Float precioPropiedad;
    //private List<Alquiler> listaDeAlquileresDePropiedad;

    public Propiedad(Ubicacion _ubicacionDeLaPropiedad ,Float _metrosCuadradosDeLapropiedad, 
                    String _estadoDeLaPropiedad, Float precioPropiedad){
        this.ubicacionDePropiedad = _ubicacionDeLaPropiedad;
        this.metrosCuadradosDePropiedad = _metrosCuadradosDeLapropiedad;
        this.estadoDeLaPropiedad = _estadoDeLaPropiedad.trim().toUpperCase(); //quitamos los espacios en blanco " " y pasamos el String a mayusculas. 
        this.precioPropiedad = precioPropiedad; 
        //this.listaDeAlquileresDePropiedad = new ArrayList<Alquiler>();
    }
    

    public Propiedad() {
    }


    //metodos set               
    public void setMetrosCuadradosDeLaPropiedad(Float _metrosCuadradosDeLapropiedad){
        this.metrosCuadradosDePropiedad = _metrosCuadradosDeLapropiedad;
    }
    
    public void setEstadoDeLaPropiedad(String _estadoDeLaPropiedad){
        this.estadoDeLaPropiedad = _estadoDeLaPropiedad;
    }

    public void setPrecioDeLaPropiedad(Float nuevoPrecio){
        this.precioPropiedad = nuevoPrecio;
    }

    //metodos get
    public Float getMetrosCuadradosDeLaPropiedad(){
        return metrosCuadradosDePropiedad;
    }
    public Ubicacion getUbicacionDePropiedad(){ 
        return ubicacionDePropiedad;
    }
    public Float getPrecioDePropiedad(){
        return precioPropiedad;
    }
    public String getEstadoDeLaPropiedad(){
        return estadoDeLaPropiedad;
    }

}


















