package inmobiliaria;

public class Ubicacion {
    private String localidad;
    private String barrio;
    private String direccion;
    

    public Ubicacion(String localidad, String barrio, String direccion){
        this.localidad = localidad.toUpperCase();
        this.barrio = barrio.toUpperCase();
        this.direccion = direccion.toUpperCase();
    }

    public String toString(){
        return "Mi ubicacion";
    }

    public String getLocalidad(){
        return localidad;
    }

    public String getBarrio(){
        return barrio;
    }

    public String getDireccion(){
        return direccion;
    }

}
