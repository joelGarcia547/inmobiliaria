package inmobiliaria;

//import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import java.util.List;

//import java.util.ArrayList;

import org.junit.Test;

import Exceptions.*;

public class CasaTest {
    @Test
    public void testCantidadDeAmbientes(){
        Ubicacion ubicacion = new Ubicacion("Catamarca Capital", "Ojo de Agua", 
        "Cabildo abierto 555");
        Casa casaAuxiliar = new Casa(5, ubicacion, 20.5f, "vendida", 5600f);

        assertEquals(5, casaAuxiliar.getCantidadDeAmbientes(),0);
    }

    @Test
    public void verificarAgregadoDeAmbienteALaListaDeLaCasa(){
        Ubicacion ubicacion = new Ubicacion("Polcos", "general lopez y ruiz", "lopez 678 - casa 25");
        Casa casa = new Casa(4, ubicacion, 23f, "Alquilada", 45000f);
        Ambiente ambienteA = new Ambiente("Lavanderia", 2.5f);
        Ambiente ambienteB = new Ambiente("mini almacen", 1.8f);
        Ambiente ambienteC = new Ambiente("Baño", 2.5F);
        Ambiente ambienteD = new Ambiente("Habitacion", 3.5F);

        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambienteA);
            casa.agregarAmbienteALaListaDeLaCasa(ambienteB);
            casa.agregarAmbienteALaListaDeLaCasa(ambienteC);
            casa.agregarAmbienteALaListaDeLaCasa(ambienteD);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        List<Ambiente> listaQueMeLlega = null;
        listaQueMeLlega = casa.getListaDeAmbientes();

        assertEquals(4, listaQueMeLlega.size());
    }
    @Test(expected = ListaDeAmbientesLlenaException.class)
    public void testAgregarMasAmbientesALaLista() throws ListaDeAmbientesLlenaException{
        Ubicacion ubicacion = new Ubicacion("Polcos", "general lopez y ruiz", "lopez 678 - casa 25");
        Casa casa = new Casa(3, ubicacion, 23f, "Alquilada", 45000f);
        Ambiente ambienteA = new Ambiente("Lavanderia", 2.5f);
        Ambiente ambienteB = new Ambiente("mini almacen", 1.8f);
        Ambiente ambienteC = new Ambiente("Lavanderia", 2.5f);
        Ambiente ambienteD = new Ambiente("Habitacion", 3F);

        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambienteA);
            casa.agregarAmbienteALaListaDeLaCasa(ambienteB);
            casa.agregarAmbienteALaListaDeLaCasa(ambienteC);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }
        casa.agregarAmbienteALaListaDeLaCasa(ambienteD);
    }

    @Test
    public void testBuscarAmbienteEnLista(){
        Ubicacion ubicacion = new Ubicacion("La merced", "jose lopez", "av fernandez - lopez a altura 450");
        Casa casa = new Casa(3, ubicacion, 25.5f, "disponible", 65000f);
        Ambiente ambiente0 = new Ambiente("Living", 3f);
        Ambiente ambiente1 = new Ambiente("Habitacion de huespedes", 4.5f);
        Ambiente ambiente2 = new Ambiente("Baño", 3.5F);

        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambiente0);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente1);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente2);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        Ambiente ambienteAux = null;
        try {
            ambienteAux = casa.buscarAmbienteEnLista(ambiente1);
        } catch (AmbienteInexistenteEnListaException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(ambiente1, ambienteAux);
    }

    @Test (expected = AmbienteInexistenteEnListaException.class)
    public void testBuscarAmbienteInexistente() throws AmbienteInexistenteEnListaException{
        Ubicacion ubicacion = new Ubicacion("La merced", "jose lopez", "av fernandez - lopez a altura 450");
        Casa casa = new Casa(3, ubicacion, 25.5f, "disponible", 65000f);
        Ambiente ambiente0 = new Ambiente("Living", 3f);
        Ambiente ambiente1 = new Ambiente("Habitacion de huespedes", 4.5f);
        Ambiente ambiente2 = new Ambiente("Baño", 3.5F);
        Ambiente ambienteD = new Ambiente("Habitacion", 3F);

        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambiente0);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente1);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente2);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        
        casa.buscarAmbienteEnLista(ambienteD);
    }

    @Test
    public void testVerificarAmbiente(){
        Ubicacion ubicacion = new Ubicacion("La banda","la tablada", "S/N - casa 28");
        Casa casa = new Casa(3, ubicacion, 30f, "Vendida", 58000f);
        Ambiente ambiente0 = new Ambiente("Living", 3f);
        Ambiente ambiente1 = new Ambiente("Habitacion de huespedes", 4.5f);
        Ambiente ambiente2 = new Ambiente("Baño", 3.5F);

        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambiente0);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente1);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente2);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        Boolean valorQueEspero = true, valorQueLlego = casa.verificarAmbiente(ambiente1);

        assertEquals(valorQueEspero, valorQueLlego);
    }
    @Test
    public void testVerificarAmbienteInexistente(){
        Ubicacion ubicacion = new Ubicacion("La banda","la tablada", "S/N - casa 28");
        Casa casa = new Casa(3, ubicacion, 30f, "Vendida", 58000f);
        Ambiente ambiente0 = new Ambiente("Living", 3f);
        Ambiente ambiente1 = new Ambiente("Habitacion de huespedes", 4.5f);
        Ambiente ambiente2 = new Ambiente("Baño", 3.5F);

        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambiente0);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente1);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        Boolean valorQueEspero = false, valorQueLlego = casa.verificarAmbiente(ambiente2);

        assertEquals(valorQueEspero, valorQueLlego);
    }

    @Test
    public void testEliminarAmbiente(){
        Ubicacion ubicacion = new Ubicacion("La banda","la tablada", "S/N - casa 28");
        Casa casa = new Casa(3, ubicacion, 30f, "Vendida", 58000f);
        Ambiente ambiente0 = new Ambiente("Living", 3f);
        Ambiente ambiente1 = new Ambiente("Habitacion de huespedes", 4.5f);
        Ambiente ambiente2 = new Ambiente("Baño", 3.5F);

        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambiente0);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente1);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente2);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        try {
            casa.eliminarAmbiente(ambiente0);
        } catch (AmbienteInexistenteEnListaException e) {
            System.out.println(e.getMessage());
        }

        Integer cantidadDeAmbientesQueEspero = 2, cantidadQueMeLlega = 0;
        List<Ambiente> listaDeAmbientes = casa.getListaDeAmbientes();
        cantidadQueMeLlega = listaDeAmbientes.size();

        //assertEquals(cantidadDeAmbientesQueEspero, casa.getCantidadDeAmbientes());
        assertEquals(cantidadDeAmbientesQueEspero, cantidadQueMeLlega); //de cualquiera de las 2 formas esta correcto
    }

    @Test (expected = AmbienteInexistenteEnListaException.class)
    public void testEliminarAmbienteInexistente() throws AmbienteInexistenteEnListaException{
        Ubicacion ubicacion = new Ubicacion("La banda","la tablada", "S/N - casa 28");
        Casa casa = new Casa(3, ubicacion, 30f, "Vendida", 58000f);
        Ambiente ambiente0 = new Ambiente("Living", 3f);
        Ambiente ambiente1 = new Ambiente("Habitacion de huespedes", 4.5f);
        Ambiente ambiente2 = new Ambiente("Baño", 3.5F);
        Ambiente ambienteD = new Ambiente("Habitacion", 3F);

        try {
            casa.agregarAmbienteALaListaDeLaCasa(ambiente0);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente1);
            casa.agregarAmbienteALaListaDeLaCasa(ambiente2);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        casa.eliminarAmbiente(ambienteD);
    }

}
