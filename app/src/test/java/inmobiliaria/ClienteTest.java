package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ClienteTest {
    @Test
    public void verificarRetornoToString(){
        //Cliente cliente = new Cliente();
        Ubicacion ubicacion = new Ubicacion("Catamarca capital", "Choya", "av virgen del valle 555");
        Cliente cliente2 = new Cliente("Carlos", "Garcia", "43789139",
        "carlosg25@gmail.com","+54383896434", ubicacion, 15016);

        String mensajeCliente = cliente2.toString();

        assertEquals("datos del cliente", mensajeCliente);
    }
}
