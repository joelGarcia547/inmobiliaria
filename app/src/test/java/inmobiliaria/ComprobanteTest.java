package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class ComprobanteTest {
    @Test
    public void verificarRetornoToString(){
        //Comprobante factura = new Comprobante();
        Propiedad propiedad = new Propiedad();
        Comprobante factura = new Comprobante(propiedad, null, null, null, null, 0000);
        String mensajeComprobante = factura.toString();

        assertEquals("Su pago fue efectuado correctamente", mensajeComprobante);
    }

    @Test
    public void verificarAgregadoDeNuevaPropiedadAFacturar(){
        Ubicacion ubicacion = new Ubicacion("Capital", "40vv norte", "guzman altura 1900");
        Ubicacion ubicacionCliente = new Ubicacion("Andalgala", "Santa Rosa", "Casa N°14");
        Propiedad propiedad = new Propiedad(ubicacion, 36.7f, "Disponible", 50000f);
        Cliente cliente = new Cliente("Romeo", "Felindez", "43678423", "felindez_@gmail.com", "3836709438", ubicacionCliente, 1001);
        Comprobante factura = new Comprobante(propiedad, 5000f, null, LocalDate.now(), cliente, 0001);
    
        factura.agregarNuevaPropiedadAFacturar(propiedad, 2);

        assertEquals(1, factura.getPropiedadesAFacturar().size());
    }

    @Test
    public void verificarPrecioTotal(){
        Ubicacion ubicacion = new Ubicacion("Capital", "40vv norte", "guzman altura 1900");
        Ubicacion ubicacionCliente = new Ubicacion("Andalgala", "Santa Rosa", "Casa N°14");
        Propiedad propiedad = new Propiedad(ubicacion, 36.7f, "Disponible", 50000f);
        Cliente cliente = new Cliente("Romeo", "Felindez", "43678423", "felindez_@gmail.com", "3836709438", ubicacionCliente, 1001);
        Comprobante factura = new Comprobante(propiedad, 5000f, null, LocalDate.now(), cliente, 0001);
    
        factura.agregarNuevaPropiedadAFacturar(propiedad, 4);

        assertEquals(200000.4f, factura.calcularPrecioTotal(),0.5f);
    }
    
    //testear clases internas tambien? .joel
}
