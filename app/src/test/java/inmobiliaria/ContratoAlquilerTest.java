package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class ContratoAlquilerTest {
  @Test
  public void verificarAgregadoDeNuevoContratoDeAlquiler(){
    //Float operacionAlquiler = 7500f;
    Ubicacion ubicacion = new Ubicacion("Valle viejo", "eusebio ruzo", "casa 17 - s/n");
    Ubicacion ubicacion2 = new Ubicacion("Valle chico", "45 vv norte", "felipe carrizo 470 - casa 20");
    //Propiedad propiedad = new Propiedad(ubicacion, 42.5f, " no disponible", 42000f);
    Propiedad propiedad2 = new Propiedad(ubicacion2, 30.5f, "disponible", 38000f);
    LocalDate fechaDeInicioContrato = LocalDate.of(2022, 3, 25);
    LocalDate fechaDeFinalizacionContrato = LocalDate.of(2023, 3, 24);
    LocalDate fechaDeOperacionDeAlquiler = LocalDate.now();
    Cliente cliente = new Cliente("Carlos", "Castillo", "43564789", "castillo_@gmail.com", "3834567812", ubicacion, 45);

    Integer cantidadDeTiempoDeEstadia = 364; //diferencia entre final y inicio en dias (365 - 1)
    Float precioPactado = (propiedad2.getPrecioDePropiedad() / cantidadDeTiempoDeEstadia); 
  
    ContratoDeAlquiler contrato = new ContratoDeAlquiler(propiedad2, true, fechaDeInicioContrato, fechaDeFinalizacionContrato, fechaDeOperacionDeAlquiler, precioPactado, cliente, "en dias");

    assertEquals("alquilada", contrato.getContratoDeAlquiler().getPropiedadAAlquilar().getEstadoDeLaPropiedad());
  }  

  @Test
  public void verificarEstadiaDelCliente(){
    Ubicacion ubicacion = new Ubicacion("Valle viejo", "eusebio ruzo", "casa 17 - s/n");
    Ubicacion ubicacion2 = new Ubicacion("Valle chico", "45 vv norte", "felipe carrizo 470 - casa 20");
    Propiedad propiedad2 = new Propiedad(ubicacion2, 30.5f, "disponible", 38000f);
    LocalDate fechaDeInicioContrato = LocalDate.of(2022, 3, 25);
    LocalDate fechaDeFinalizacionContrato = LocalDate.of(2023, 3, 24);
    LocalDate fechaDeOperacionDeAlquiler = LocalDate.now();
    Cliente cliente = new Cliente("Carlos", "Castillo", "43564789", "castillo_@gmail.com", "3834567812", ubicacion, 45);
    Float precioPactadoEnDias = 2500f; 
  
    ContratoDeAlquiler contratoMensual = new ContratoDeAlquiler(propiedad2, true, fechaDeInicioContrato, fechaDeFinalizacionContrato, fechaDeOperacionDeAlquiler, precioPactadoEnDias, cliente, "mensual");
    ContratoDeAlquiler contratoTemporal = new ContratoDeAlquiler(propiedad2, true, fechaDeInicioContrato, fechaDeFinalizacionContrato, fechaDeOperacionDeAlquiler, precioPactadoEnDias, cliente, "temporal");


    Integer estadiaEnMeses = contratoMensual.getContratoDeAlquiler().determinarEstadia();
    Integer estadiaEnDias = contratoTemporal.getContratoDeAlquiler().determinarEstadia();

    assertEquals(11, estadiaEnMeses, 0);
    //assertEquals(27, estadiaEnDias, 0);
  }

  @Test
  public void verificarPrecioTotalDeEstadia(){
    Ubicacion ubicacion = new Ubicacion("Valle viejo", "eusebio ruzo", "casa 17 - s/n");
    Ubicacion ubicacion2 = new Ubicacion("Valle chico", "45 vv norte", "felipe carrizo 470 - casa 20");
    Propiedad propiedad2 = new Propiedad(ubicacion2, 30.5f, "disponible", 38000f);
    LocalDate fechaDeInicioContrato = LocalDate.of(2022, 3, 25);
    LocalDate fechaDeFinalizacionContrato = LocalDate.of(2023, 3, 24);
    LocalDate fechaDeOperacionDeAlquiler = LocalDate.now();
    Cliente cliente = new Cliente("Carlos", "Castillo", "43564789", "castillo_@gmail.com", "3834567812", ubicacion, 45);
    Float precioPactadoEnDias = 1500f; 
  
    ContratoDeAlquiler contratoMensual = new ContratoDeAlquiler(propiedad2, true, fechaDeInicioContrato, fechaDeFinalizacionContrato, fechaDeOperacionDeAlquiler, precioPactadoEnDias, cliente, "mensual");
    ContratoDeAlquiler contratoTemporal = new ContratoDeAlquiler(propiedad2, true, fechaDeInicioContrato, fechaDeFinalizacionContrato, fechaDeOperacionDeAlquiler, precioPactadoEnDias, cliente, "temporal");

    Float toallaYSabanaNoIncluido = contratoMensual.getContratoDeAlquiler().calcularPrecioTotalEstadia(false, 600f);
    Float toallaYSabanaIncluido = contratoTemporal.getContratoDeAlquiler().calcularPrecioTotalEstadia(true, 800f);

    //assertEquals(495000.4, toallaYSabanaNoIncluido, 0.5f);
    assertEquals(41300.4, toallaYSabanaIncluido, 0.5f);
  }


}
