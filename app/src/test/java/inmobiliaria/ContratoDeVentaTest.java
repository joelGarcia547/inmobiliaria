package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class ContratoDeVentaTest {
    @Test
    public void verificarCreacionDeNuevoContratoDeVenta(){
        Float costoDeOperacion = 5000f;
        Ubicacion ubicacionCliente = new Ubicacion("Polcos", "Los aroldos", "San roberto martinez 356");
        Ubicacion ubicacionDePropiedad = new Ubicacion("San isidro", "los mimbres", "Eusebio ruzo - s/n casa 23"); 
        Propiedad propiedadAVender1 = new Propiedad(ubicacionDePropiedad, 45.6f, "DISPONIBLE", 45000f);
        //Propiedad propiedadAVender2 = new Propiedad(ubicacionDePropiedad, 55.6f, "NO DISPONIBLE", 55000f);
        Cliente cliente = new Cliente("Carlos", "Castillo", "43564789", "castillo_@gmail.com", "3834567812", ubicacionCliente, 45);

        Float precioPactadoDeVenta1 = (propiedadAVender1.getPrecioDePropiedad() + costoDeOperacion); //costo de operacion que puede variar
        //Float precioPactadoDeVenta2 = (propiedadAVender2.getPrecioDePropiedad() + costoDeOperacion); //costo de operacion que puede variar
       
        ContratoDeVenta contrato = new ContratoDeVenta(propiedadAVender1, precioPactadoDeVenta1, true, cliente, LocalDate.of(2021, 3, 22));
        //ContratoDeVenta contrato2 = new ContratoDeVenta(propiedadAVender2, precioPactadoDeVenta2, false, cliente, LocalDate.now());

        assertEquals("vendida", contrato.getComprobanteDeVenta().getFacturaDeVenta().getPropiedadAFacturar().getEstadoDeLaPropiedad());
        //assertNotEquals(LocalDate.now(), contrato2.getComprobanteDeVenta().getFacturaDeVenta().getFechaDeLaOperacionActual());

    }

   
}
