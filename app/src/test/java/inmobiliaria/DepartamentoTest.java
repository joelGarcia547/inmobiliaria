package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.util.List;


import org.junit.Test;

import Exceptions.AmbienteInexistenteEnListaException;
import Exceptions.ListaDeAmbientesLlenaException;

public class DepartamentoTest {
    
    @Test
    public void testPisoDelDepartamento(){
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5,3, true, 4, 
                  ubicacion, 20F, "disponible", 30000F);

        Integer pisoQueEspero = 5, pisoQueLLega = departamento.getPisoDelDepartamento();

        assertEquals(pisoQueEspero, pisoQueLLega);
    }

    @Test
    public void testCocheraDelDepartamento(){
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5,3, true, 4, 
                  ubicacion, 20F, "disponible", 30000F);

        Boolean cochera = departamento.getCocheraDelDepartamento();

        assertEquals(true, cochera);
    }

    @Test
    public void testCantidadDeAmbientesDelDepartamento(){
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 4, 
                  ubicacion, 20F, "disponible", 30000F);

        Integer cantidadDeAmbientesQueEspero = 4, cantidadQueMeLlega = departamento.getCantidadDeAmbientes();

        assertEquals(cantidadDeAmbientesQueEspero, cantidadQueMeLlega);
    }

    @Test
    public void testAgregarAmbientesAlDpto(){
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 4, 
                  ubicacion, 20F, "disponible", 30000F);
        Ambiente ambiente1 = new Ambiente("Habitacion", 3F);
        Ambiente ambiente2 = new Ambiente("Baño", 2.5F);
        Ambiente ambiente3 = new Ambiente("Cocina-Comedor", 7F);
        Ambiente ambiente4 = new Ambiente("Balcon", 2.5F);

        try {
            departamento.agregarAmbienteAlDpto(ambiente1);
            departamento.agregarAmbienteAlDpto(ambiente2);
            departamento.agregarAmbienteAlDpto(ambiente3);
            departamento.agregarAmbienteAlDpto(ambiente4);

        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        Integer cantidadDeAmbientesAgregados = 4, cantidadQueMeLlega = 0;
        List<Ambiente> listaDeAmbientesDelDpto = departamento.getListaDeAmbientesDelDpto();
        cantidadQueMeLlega = listaDeAmbientesDelDpto.size();

        assertEquals(cantidadDeAmbientesAgregados, cantidadQueMeLlega);
    }
    @Test (expected = ListaDeAmbientesLlenaException.class)
    public void testAgregarMasAmbientesALaLista() throws ListaDeAmbientesLlenaException{
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 3, 
                  ubicacion, 20F, "disponible", 30000F);
        Ambiente ambiente1 = new Ambiente("Habitacion", 3F);
        Ambiente ambiente2 = new Ambiente("Baño", 2.5F);
        Ambiente ambiente3 = new Ambiente("Cocina-Comedor", 7F);
        Ambiente ambiente4 = new Ambiente("Balcon", 2.5F);

        try {
            departamento.agregarAmbienteAlDpto(ambiente1);
            departamento.agregarAmbienteAlDpto(ambiente2);
            departamento.agregarAmbienteAlDpto(ambiente3);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        departamento.agregarAmbienteAlDpto(ambiente4);
    }

    @Test
    public void testVerificarAmbiente(){
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 4, 
                  ubicacion, 20F, "disponible", 30000F);
        Ambiente ambiente1 = new Ambiente("Habitacion", 3F);
        Ambiente ambiente2 = new Ambiente("Baño", 2.5F);

        try {
            departamento.agregarAmbienteAlDpto(ambiente1);
            departamento.agregarAmbienteAlDpto(ambiente2);

        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        Boolean valorQueNosDevuelve = departamento.verificarAmbiente(ambiente1);

        assertEquals(true, valorQueNosDevuelve);
    }

    @Test
    public void testVerificarAmbienteInexistente(){
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 4, 
                  ubicacion, 20F, "disponible", 30000F);
        Ambiente ambiente1 = new Ambiente("Habitacion", 3F);
        Ambiente ambiente2 = new Ambiente("Baño", 2.5F);
        Ambiente ambiente3 = new Ambiente("Cocina-Comedor", 7F);
        Ambiente ambiente4 = new Ambiente("Balcon", 2.5F);

        try {
            departamento.agregarAmbienteAlDpto(ambiente1);
            departamento.agregarAmbienteAlDpto(ambiente2);
            departamento.agregarAmbienteAlDpto(ambiente3);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        Boolean valorQueDevuelve = departamento.verificarAmbiente(ambiente4);
        assertEquals(false, valorQueDevuelve);
    }

    @Test
    public void testEliminarAmbienteDeLaLista(){
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 4, 
                  ubicacion, 20F, "disponible", 30000F);
        Ambiente ambiente1 = new Ambiente("Habitacion", 3F);
        Ambiente ambiente2 = new Ambiente("Baño", 2.5F);
        Ambiente ambiente3 = new Ambiente("Cocina-Comedor", 7F);
        Ambiente ambiente4 = new Ambiente("Balcon", 2.5F);

        try {
            departamento.agregarAmbienteAlDpto(ambiente1);
            departamento.agregarAmbienteAlDpto(ambiente2);
            departamento.agregarAmbienteAlDpto(ambiente3);
            departamento.agregarAmbienteAlDpto(ambiente4);

        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        try {
            departamento.eliminarAmbienteDelDpto(ambiente4);
            departamento.eliminarAmbienteDelDpto(ambiente2);
        } catch (AmbienteInexistenteEnListaException e) {
            System.out.println(e.getMessage());
        }

        Integer cantidadQuedeberiaTener = 2, cantidadQueNosLlega = departamento.getCantidadDeAmbientes();
        assertEquals(cantidadQuedeberiaTener, cantidadQueNosLlega);
    }

    @Test (expected = AmbienteInexistenteEnListaException.class)
    public void testEliminarAmbienteInexistente() throws AmbienteInexistenteEnListaException{
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 4, 
                  ubicacion, 20F, "disponible", 30000F);
        Ambiente ambiente1 = new Ambiente("Habitacion", 3F);
        Ambiente ambiente2 = new Ambiente("Baño", 2.5F);
        Ambiente ambiente3 = new Ambiente("Cocina-Comedor", 7F);
        Ambiente ambiente4 = new Ambiente("Balcon", 2.5F);

        try {
            departamento.agregarAmbienteAlDpto(ambiente1);
            departamento.agregarAmbienteAlDpto(ambiente2);
            departamento.agregarAmbienteAlDpto(ambiente3);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        departamento.eliminarAmbienteDelDpto(ambiente4);
    }

    @Test
    public void testBuscarAmbienteEnLaLista(){
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 4, 
                  ubicacion, 20F, "disponible", 30000F);
        Ambiente ambiente1 = new Ambiente("Habitacion", 3F);
        Ambiente ambiente2 = new Ambiente("Baño", 2.5F);
        Ambiente ambiente3 = new Ambiente("Cocina-Comedor", 7F);
        Ambiente ambiente4 = new Ambiente("Balcon", 2.5F);

        try {
            departamento.agregarAmbienteAlDpto(ambiente1);
            departamento.agregarAmbienteAlDpto(ambiente2);
            departamento.agregarAmbienteAlDpto(ambiente3);
            departamento.agregarAmbienteAlDpto(ambiente4);

        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        Ambiente ambienteQueEspero = ambiente2, ambienteQueBusco = null;

        try {
            ambienteQueBusco = departamento.buscarAmbienteEnLaLista(ambiente2);
        } catch (AmbienteInexistenteEnListaException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(ambienteQueEspero, ambienteQueBusco);
    }

    @Test (expected = AmbienteInexistenteEnListaException.class)
    public void testBuscarAmbienteInexistenteEnLaLista() throws AmbienteInexistenteEnListaException{
        Ubicacion ubicacion = new Ubicacion("San Fernando del Valle de Catamarca", "Circulo medico", "Calle Reserva 555'");
        Departamento departamento = new Departamento(5, 3,true, 4, 
                  ubicacion, 20F, "disponible", 30000F);
        Ambiente ambiente1 = new Ambiente("Habitacion", 3F);
        Ambiente ambiente2 = new Ambiente("Baño", 2.5F);
        Ambiente ambiente3 = new Ambiente("Cocina-Comedor", 7F);
        Ambiente ambiente4 = new Ambiente("Balcon", 2.5F);

        try {
            departamento.agregarAmbienteAlDpto(ambiente1);
            departamento.agregarAmbienteAlDpto(ambiente2);
            departamento.agregarAmbienteAlDpto(ambiente3);
        } catch (ListaDeAmbientesLlenaException e2){
            System.out.println(e2.getMessage());
        }

        departamento.buscarAmbienteEnLaLista(ambiente4);
    }
}
