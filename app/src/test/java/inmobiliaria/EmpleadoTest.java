package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EmpleadoTest {
    @Test
    public void verificarRetornoToString(){
        //Empleado empleado = new Empleado();
        Ubicacion ubicacion = new Ubicacion("Catamarca capital", "Valle chico", "Calle 45 Casa NÂ°80");
        Empleado empleado1 = new Empleado("Ramiro", "Paredes", "44176895", "ramiro56@gmail.com", 
                              "+543834648921",ubicacion,"Ventas");
        String mensajeEmpleado = empleado1.toString();

        assertEquals("datos del empleado", mensajeEmpleado);
    }

    /*@Test
    public void testNumeroDeLegajo(){
        Ubicacion ubicacion = new Ubicacion("Catamarca capital", "Valle chico", "Calle 45 Casa NÂ°80");
        Empleado empleado2 = new Empleado("Ramiro", "Paredes", "44176895", "ramiro56@gmail.com", 
                              "+543834648921",ubicacion,"Ventas");
        Integer numeroDeLegajo = empleado2.getNumeroDeLegajo();
        Integer numeroQueEspero = 0002;

        assertEquals(numeroQueEspero, numeroDeLegajo);
    }*/

    /*@Test
    public void testNumerosDeLegajo(){
        Ubicacion ubicacion = new Ubicacion("Catamarca capital", "Valle chico", "Calle 45 Casa NÂ°80");
        Ubicacion ubicacion2 = new Ubicacion("San Fernadno del Valle", " ", "calle san martin 333");
        Empleado empleado3 = new Empleado("Ramiro", "Paredes", "44176895", "ramiro56@gmail.com", 
                              "+543834648921",ubicacion,"Ventas");
        Empleado empleado4 = new Empleado("Lucia", "Gomez", "87654321", 
                "lucia@gamil.com", "3834123456", ubicacion2, 
                "Ventas");
        
        Integer numeroDeLegajo = empleado4.getNumeroDeLegajo();
        Integer numeroQueEspero = 0004;

        assertEquals(numeroQueEspero, numeroDeLegajo);
    }*/
}
