package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class InmobiliariaTest {
    @Test
    public void testNombreDeLaInmobiliaria(){
        Ubicacion ubicacion = new Ubicacion("Catamarca capital", "El Jumeal", 
        "Av. Siempre Viva 555");
        Inmobiliaria inmobiliariaAux = new Inmobiliaria("MIKSA", 
        ubicacion, "correo@gmail.com", 
        "3834123456");

        String nombreAuxiliar;

        nombreAuxiliar = inmobiliariaAux.getNombreDeInmobiliaria();

        assertEquals("MIKSA", nombreAuxiliar);
    }

}
