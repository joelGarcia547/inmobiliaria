package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import Exceptions.ClienteExistenteEnRegistroException;
import Exceptions.ClienteInexistenteEnRegistroException;

public class LibroClienteTest {
    @Test
    public void verificarAgregadoDeClienteARegistro(){
        Ubicacion ubicacion1 = new Ubicacion("Catamarca capital", "Choya", "av virgen del valle 555");
        Ubicacion ubicacion2 = new Ubicacion("Andalgala", "Centro", "San martin 414");
        Cliente cliente1 = new Cliente("Nelson", "Vega", "43768324", "vega_@gmail.com", 
                                    "3834658923", ubicacion1, 0001);
        Cliente cliente2 = new Cliente("Julieta", "Rivas", "12345678", "julieta@gmail.com", 
                                    "3834123456" , ubicacion2, 0002);
        LibroCliente libro = new LibroCliente();
        
        try {
            libro.agregarClienteARegistro(cliente1);
            libro.agregarClienteARegistro(cliente2);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        List<Cliente> listaQueMeLLega = libro.getRegistroDeClientes();
        assertEquals(2, listaQueMeLLega.size());
    }

    @Test (expected = ClienteExistenteEnRegistroException.class)
    public void testAgregarElMismoCliente() throws ClienteExistenteEnRegistroException{
        Ubicacion ubicacion1 = new Ubicacion("Catamarca capital", "Choya", "av virgen del valle 555");
        Cliente cliente1 = new Cliente("Nelson", "Vega", "43768324", "vega_@gmail.com", 
                                    "3834658923", ubicacion1, 0001);
        Cliente cliente2 = new Cliente("Nelson", "Vega", "43768324", "vega_@gmail.com", 
                                     "3834658923", ubicacion1, 0001);
        LibroCliente libro = new LibroCliente();
        //try {
            libro.agregarClienteARegistro(cliente1);
            libro.agregarClienteARegistro(cliente2);
        //} catch (ClienteExistenteEnRegistroException e) {
            //System.out.println(e.getMessage());
        //}
    }

    @Test
    public void verificarBusquedaDeClienteEnRegistro(){
        Ubicacion ubicacion1 = new Ubicacion("Catamarca capital", "Choya", "av virgen del valle 555");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca capital", "Valle chico", "Calle 45 Casa N°80");
        Cliente clienteA = new Cliente("Nelson", "Vega", "43768324", "vega_@gmail.com", 
                    "3834658923", ubicacion1, 0010);
        Cliente clienteB = new Cliente("Fernando", "tapia", "43345234", "tapiaF_@gmail.com", 
                    "3835435645", ubicacion2, 0011);
        LibroCliente libro = new LibroCliente();

        try {
            libro.agregarClienteARegistro(clienteA);
            libro.agregarClienteARegistro(clienteB);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        Cliente clienteQueMeLlega = null;

        try {
            clienteQueMeLlega = libro.buscarClienteEnRegistro("43345234");
        } catch (ClienteInexistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }
        assertEquals(clienteB, clienteQueMeLlega);
    }

    @Test (expected = ClienteInexistenteEnRegistroException.class)
    public void testBuscarClienteInexistente() throws ClienteInexistenteEnRegistroException{
        Ubicacion ubicacion1 = new Ubicacion("Catamarca capital", "Choya", "av virgen del valle 555");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca capital", "Valle chico", "Calle 45 Casa N°80");
        Cliente clienteA = new Cliente("Nelson", "Vega", "43768324", "vega_@gmail.com", 
                    "3834658923", ubicacion1, 0010);
        Cliente clienteB = new Cliente("Fernando", "tapia", "43345234", "tapiaF_@gmail.com", 
                    "3835435645", ubicacion2, 0011);
        LibroCliente libro = new LibroCliente();

        try {
            libro.agregarClienteARegistro(clienteA);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        libro.buscarClienteEnRegistro(clienteB.getDNIdeLaPersona());
        
    }

    @Test
    public void verificarEliminacionDeClienteDeRegistro(){
        Ubicacion ubicacion1 = new Ubicacion("Catamarca capital", "Choya", "av virgen del valle 555");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca capital", "Valle chico", "Calle 45 Casa N°80");
        Cliente clienteA = new Cliente("Nelson", "Vega", "43768324", "vega_@gmail.com", 
                    "3834658923", ubicacion1, 0010);
        Cliente clienteB = new Cliente("Fernando", "tapia", "43345234", "tapiaF_@gmail.com", 
                    "3835435645", ubicacion2, 0011);
        LibroCliente libro = new LibroCliente();

        try {
            libro.agregarClienteARegistro(clienteA);
            libro.agregarClienteARegistro(clienteB);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }
        
        try {
            libro.removerClienteDeRegistro("43768324");
        } catch (ClienteInexistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(1, libro.getRegistroDeClientes().size());
    }

    @Test (expected = ClienteInexistenteEnRegistroException.class)
    public void testEliminarClienteInexistente() throws ClienteInexistenteEnRegistroException{
        Ubicacion ubicacion1 = new Ubicacion("Catamarca capital", "Choya", "av virgen del valle 555");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca capital", "Valle chico", "Calle 45 Casa N°80");
        Cliente clienteA = new Cliente("Nelson", "Vega", "43768324", "vega_@gmail.com", 
                    "3834658923", ubicacion1, 0010);
        Cliente clienteB = new Cliente("Fernando", "tapia", "43345234", "tapiaF_@gmail.com", 
                    "3835435645", ubicacion2, 0011);
        LibroCliente libro = new LibroCliente();

        try {
            libro.agregarClienteARegistro(clienteA);
            //libro.agregarClienteARegistro(clienteB);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        libro.removerClienteDeRegistro(clienteB.getDNIdeLaPersona());

    }
}
