package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import Exceptions.ContratoDeAlquilerInexistenteException;
import Exceptions.ContratoDeAlquilerYaExistenteException;

public class LibroDeAlquileresTest {
    @Test
    public void verificarAgregadoDeContratoDeAlquileresAlRegistro(){
        Float operacion = 11750f;
        Ubicacion ubicacionCliente = new Ubicacion("San jose paz", "Dr. antonio savedra", "savedra 789 - casa 34");
        Cliente clienteDeVenta = new Cliente("Roberto", "Martinez", "43489231", "MartinezRoberto_@gmail.com", "3834561222", ubicacionCliente, 67);
        Ubicacion ubicacionPropiedad = new Ubicacion("Valle chico", "Gutierrez savedra", "av. torres al 788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 48.5f, "disponible", 57500f);
        Float precioPactadoPorDia = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeAlquiler contrato = new ContratoDeAlquiler(propiedad, true, LocalDate.of(2021, 5, 17), LocalDate.of(2021, 10, 18), LocalDate.of(2021, 10, 11), precioPactadoPorDia, clienteDeVenta, "temporal");
        LibroDeAlquileres registroDeAlquileres = new LibroDeAlquileres();

        try {
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
    
        } catch (ContratoDeAlquilerYaExistenteException e) {
            e.printStackTrace();
        }

       assertEquals(1, registroDeAlquileres.getRegistroDeAlquileres().size());
    }

    @Test (expected = ContratoDeAlquilerYaExistenteException.class)
    public void verificarContratoDeAlquilerYaexistenteException() throws ContratoDeAlquilerYaExistenteException{ 
        Float operacion = 11750f;
        Ubicacion ubicacionCliente = new Ubicacion("San jose paz", "Dr. antonio savedra", "savedra 789 - casa 34");
        Cliente clienteDeVenta = new Cliente("Roberto", "Martinez", "43489231", "MartinezRoberto_@gmail.com", "3834561222", ubicacionCliente, 67);
        Ubicacion ubicacionPropiedad = new Ubicacion("Valle chico", "Gutierrez savedra", "av. torres al 788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 48.5f, "disponible", 57500f);
        Float precioPactadoPorDia = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeAlquiler contrato = new ContratoDeAlquiler(propiedad, true, LocalDate.of(2021, 5, 17), LocalDate.of(2021, 10, 18), LocalDate.of(2021, 10, 11), precioPactadoPorDia, clienteDeVenta, "temporal");
        LibroDeAlquileres registroDeAlquileres = new LibroDeAlquileres();

        try {
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
        } catch (ContratoDeAlquilerYaExistenteException e) {
            e.printStackTrace();
        }

        registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
    }

    @Test
    public void verificarExistenciaDeContratoDeAlquiler(){
        Float operacion = 11900f;
        Ubicacion ubicacionCliente = new Ubicacion("San jose paz", "Dr. antonio savedra", "savedra 789 - casa 34");
        Cliente clienteDeVenta = new Cliente("Roberto", "Martinez", "43489231", "MartinezRoberto_@gmail.com", "3834561222", ubicacionCliente, 67);
        Ubicacion ubicacionPropiedad = new Ubicacion("Valle chico", "Gutierrez savedra", "av. torres al 788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 48.5f, "disponible", 57500f);
        Float precioPactadoPorDia = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeAlquiler contrato = new ContratoDeAlquiler(propiedad, true, LocalDate.of(2021, 5, 17), LocalDate.of(2021, 10, 18), LocalDate.of(2021, 10, 11), precioPactadoPorDia, clienteDeVenta, "temporal");
        
        LibroDeAlquileres registroDeAlquileres = new LibroDeAlquileres();

        try {
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
        } catch (ContratoDeAlquilerYaExistenteException e) {
            e.printStackTrace();
        }

        Integer codigoFactura = contrato.getContratoDeAlquiler().getFacturaDeAlquiler().getCodigoDeOperacion();

        assertEquals(true, registroDeAlquileres.verificarExistenciaDeContratoDeAlquiler(codigoFactura));
    }

    @Test
    public void verificarBusquedaDeContratoDeAlquilerEnLibro(){
        Float operacion = 11900f;
        Ubicacion ubicacionCliente = new Ubicacion("San jose paz", "Dr. antonio savedra", "savedra 789 - casa 34");
        Cliente clienteDeVenta = new Cliente("Roberto", "Martinez", "43489231", "MartinezRoberto_@gmail.com", "3834561222", ubicacionCliente, 67);
        Ubicacion ubicacionPropiedad = new Ubicacion("Valle chico", "Gutierrez savedra", "av. torres al 788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 48.5f, "disponible", 57500f);
        Float precioPactadoPorDia = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeAlquiler contrato = new ContratoDeAlquiler(propiedad, true, LocalDate.of(2021, 5, 17), LocalDate.of(2021, 10, 18), LocalDate.of(2021, 10, 11), precioPactadoPorDia, clienteDeVenta, "temporal");
        
        LibroDeAlquileres registroDeAlquileres = new LibroDeAlquileres();

        try {
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
        } catch (ContratoDeAlquilerYaExistenteException e) {
            e.printStackTrace();
        }
         
        Integer codigoFactura = contrato.getContratoDeAlquiler().getFacturaDeAlquiler().getCodigoDeOperacion();
        ContratoDeAlquiler contratoADevolver = null;

        try {
            contratoADevolver = registroDeAlquileres.buscarContratoDeAlquilerEnLibro(codigoFactura);
        } catch (ContratoDeAlquilerInexistenteException e) {
            e.printStackTrace();
        }

        assertEquals(contrato.getContratoDeAlquiler().getFechaDeOperacionDeAlquiler(), contratoADevolver.getContratoDeAlquiler().getFechaDeOperacionDeAlquiler());
    }

    @Test (expected = ContratoDeAlquilerInexistenteException.class)
    public void verificarContratoDeAlquilerInexistenteException() throws ContratoDeAlquilerInexistenteException{
        Float operacion = 11900f;
        Ubicacion ubicacionCliente = new Ubicacion("San jose paz", "Dr. antonio savedra", "savedra 789 - casa 34");
        Cliente clienteDeVenta = new Cliente("Roberto", "Martinez", "43489231", "MartinezRoberto_@gmail.com", "3834561222", ubicacionCliente, 67);
        Ubicacion ubicacionPropiedad = new Ubicacion("Valle chico", "Gutierrez savedra", "av. torres al 788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 48.5f, "disponible", 57500f);
        Float precioPactadoPorDia = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeAlquiler contrato = new ContratoDeAlquiler(propiedad, true, LocalDate.of(2021, 5, 17), LocalDate.of(2021, 10, 18), LocalDate.of(2021, 10, 11), precioPactadoPorDia, clienteDeVenta, "temporal");
        
        LibroDeAlquileres registroDeAlquileres = new LibroDeAlquileres();

        try {
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
        } catch (ContratoDeAlquilerYaExistenteException e) {
            e.printStackTrace();
        }
        
        try {
            ContratoDeAlquiler contratoABuscar = registroDeAlquileres.buscarContratoDeAlquilerEnLibro(4578);
        } catch (ContratoDeAlquilerInexistenteException e) {
            e.printStackTrace();
        }
        //no borrar, porque sino no funciona .joel
        ContratoDeAlquiler contratoABuscar2 = registroDeAlquileres.buscarContratoDeAlquilerEnLibro(1234);
    }

    @Test
    public void verificarEliminacionContratoDeAlquilerDeRegistro(){
        Float operacion = 11900f;
        Ubicacion ubicacionCliente = new Ubicacion("San jose paz", "Dr. antonio savedra", "savedra 789 - casa 34");
        Cliente clienteDeVenta = new Cliente("Roberto", "Martinez", "43489231", "MartinezRoberto_@gmail.com", "3834561222", ubicacionCliente, 67);
        Ubicacion ubicacionPropiedad = new Ubicacion("Valle chico", "Gutierrez savedra", "av. torres al 788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 48.5f, "disponible", 57500f);
        Float precioPactadoPorDia = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeAlquiler contrato = new ContratoDeAlquiler(propiedad, true, LocalDate.of(2021, 5, 17), LocalDate.of(2021, 10, 18), LocalDate.of(2021, 10, 11), precioPactadoPorDia, clienteDeVenta, "temporal");
        
        LibroDeAlquileres registroDeAlquileres = new LibroDeAlquileres();
        
        try {
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
        } catch (ContratoDeAlquilerYaExistenteException e) {
            e.printStackTrace();
        }

        Integer codigoDeFactura = contrato.getContratoDeAlquiler().getFacturaDeAlquiler().getCodigoDeOperacion();
        registroDeAlquileres.removerContratoDeAlquilerDeRegistro(codigoDeFactura);

        assertEquals(0, registroDeAlquileres.getRegistroDeAlquileres().size());
    }

    @Test
    public void verificarModificacionDeContratoDeVenta(){
        Float operacion = 11900f;
        Ubicacion ubicacionCliente = new Ubicacion("San jose paz", "Dr. antonio savedra", "savedra 789 - casa 34");
        Cliente clienteDeVenta = new Cliente("Roberto", "Martinez", "43489231", "MartinezRoberto_@gmail.com", "3834561222", ubicacionCliente, 67);
        Ubicacion ubicacionPropiedad = new Ubicacion("Valle chico", "Gutierrez savedra", "av. torres al 788");
        Ubicacion ubicacionPropiedad2 = new Ubicacion("Valle chico", "Gutierrez savedra", "av. torres al 428");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 48.5f, "disponible", 57500f);
        Propiedad propiedad2 = new Propiedad(ubicacionPropiedad2, 42.5f, "disponible", 53300f);
        Float precioPactadoPorDia = (propiedad.getPrecioDePropiedad() / operacion);
        Float precioPactadoPorDia2 = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeAlquiler contrato = new ContratoDeAlquiler(propiedad, true, LocalDate.of(2021, 5, 17), LocalDate.of(2021, 10, 18), LocalDate.of(2021, 10, 11), precioPactadoPorDia, clienteDeVenta, "temporal");
        ContratoDeAlquiler contrato2 = new ContratoDeAlquiler(propiedad2, true, LocalDate.of(2021, 4, 17), LocalDate.of(2021,9, 18), LocalDate.of(2021, 9, 28), precioPactadoPorDia2, clienteDeVenta, "mensual");

        LibroDeAlquileres registroDeAlquileres = new LibroDeAlquileres();
        
        try {
            registroDeAlquileres.agregarContratoDeAlquilerAlRegistro(contrato);
        } catch (ContratoDeAlquilerYaExistenteException e) {
            e.printStackTrace();
        }

        registroDeAlquileres.modificarContratoDeAlquiler(contrato, contrato2);

        assertEquals(contrato2, registroDeAlquileres.getRegistroDeAlquileres().get(0));
    }
}
