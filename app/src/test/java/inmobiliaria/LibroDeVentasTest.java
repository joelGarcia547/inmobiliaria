package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import Exceptions.ContratoDeVentaInexistenteException;
import Exceptions.ContratoDeVentaYaExistenteException;

public class LibroDeVentasTest {
    @Test
    public void verificarAgregadoDeContratoDeVentaAlRegistro(){
        Float operacion = 10500f;
        Ubicacion ubicacionCliente = new Ubicacion("Chimiril", "san jose", "almirante antonio - 345 casa 34");
        Cliente clienteDeVenta = new Cliente("Maria", "Luján", "43789234", "lujan_@gmail.com", "3834567821", ubicacionCliente, 57);
        Ubicacion ubicacionPropiedad = new Ubicacion("Ojo de agua", "San Jacinto", "av. torres al 1788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 43.5f, "disponible", 47500f);
        Float precioPactadoDeVenta = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeVenta contrato1 = new ContratoDeVenta(propiedad, precioPactadoDeVenta, true, clienteDeVenta, LocalDate.of(2021, 6, 13));
        LibroDeVentas registroDeVentas = new LibroDeVentas();

        try {
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
        } catch (ContratoDeVentaYaExistenteException e) {
            e.printStackTrace();
        }

        assertEquals(1, registroDeVentas.getRegistroDeVentas().size());
    }  

    @Test (expected = ContratoDeVentaYaExistenteException.class)
    public void verificarContratoDeVentaYaExistenteException() throws ContratoDeVentaYaExistenteException{
        Float operacion = 10500f;
        Ubicacion ubicacionCliente = new Ubicacion("Chimiril", "san jose", "almirante antonio - 345 casa 34");
        Cliente clienteDeVenta = new Cliente("Maria", "Luján", "43789234", "lujan_@gmail.com", "3834567821", ubicacionCliente, 57);
        Ubicacion ubicacionPropiedad = new Ubicacion("Ojo de agua", "San Jacinto", "av. torres al 1788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 43.5f, "disponible", 47500f);
        Float precioPactadoDeVenta = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeVenta contrato1 = new ContratoDeVenta(propiedad, precioPactadoDeVenta, true, clienteDeVenta, LocalDate.of(2021, 6, 13));

        LibroDeVentas registroDeVentas = new LibroDeVentas();

        try {
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
        } catch (ContratoDeVentaYaExistenteException e) {
            e.printStackTrace();
        }
        //nose porque, pero funciona asi que no borrarlo pls .joel
           registroDeVentas.agregarContratoDeVentaARegistro(contrato1); 
    }

    @Test
    public void verificarExistenciaDeContratoDeVenta(){
        Float operacion = 10500f;
        Ubicacion ubicacionCliente = new Ubicacion("Chimiril", "san jose", "almirante antonio - 345 casa 34");
        Cliente clienteDeVenta = new Cliente("Maria", "Luján", "43789234", "lujan_@gmail.com", "3834567821", ubicacionCliente, 57);
        Ubicacion ubicacionPropiedad = new Ubicacion("Ojo de agua", "San Jacinto", "av. torres al 1788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 43.5f, "disponible", 47500f);
        Float precioPactadoDeVenta = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeVenta contrato1 = new ContratoDeVenta(propiedad, precioPactadoDeVenta, true, clienteDeVenta, LocalDate.of(2021, 6, 13));

        LibroDeVentas registroDeVentas = new LibroDeVentas();

        try {
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
        } catch (ContratoDeVentaYaExistenteException e) {
            e.printStackTrace();
        }

        Integer codigoFactura = contrato1.getComprobanteDeVenta().getFacturaDeVenta().getCodigoDeOperacion();

        assertEquals(true, registroDeVentas.verificarExistenciaDeContratoDeVenta(codigoFactura));
    }

    @Test
    public void verificarBusquedaDeContratoDeVentaEnLibro(){
        Float operacion = 10500f;
        Ubicacion ubicacionCliente = new Ubicacion("Chimiril", "san jose", "almirante antonio - 345 casa 34");
        Cliente clienteDeVenta = new Cliente("Maria", "Luján", "43789234", "lujan_@gmail.com", "3834567821", ubicacionCliente, 57);
        Ubicacion ubicacionPropiedad = new Ubicacion("Ojo de agua", "San Jacinto", "av. torres al 1788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 43.5f, "disponible", 47500f);
        Float precioPactadoDeVenta = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeVenta contrato1 = new ContratoDeVenta(propiedad, precioPactadoDeVenta, true, clienteDeVenta, LocalDate.of(2021, 6, 13));

        LibroDeVentas registroDeVentas = new LibroDeVentas();
        
        try {
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
        } catch (ContratoDeVentaYaExistenteException e) {
            e.printStackTrace();
        }
         
        Integer codigoFacturaVenta = contrato1.getComprobanteDeVenta().getFacturaDeVenta().getCodigoDeOperacion();
        ContratoDeVenta contratoADevolver = null;
        try {
            contratoADevolver = registroDeVentas.buscarContratoDeVentaEnLibro(codigoFacturaVenta);
        } catch (ContratoDeVentaInexistenteException e) {
            e.printStackTrace();
        }

        assertEquals(contrato1.getComprobanteDeVenta().getFechaDeOperacionDeVentaRealizada(), contratoADevolver.getComprobanteDeVenta().getFechaDeOperacionDeVentaRealizada());
    }

    @Test (expected = ContratoDeVentaInexistenteException.class)
    public void verificarContratoDeVentaInexistenteException() throws ContratoDeVentaInexistenteException{
        Float operacion = 10500f;
        Ubicacion ubicacionCliente = new Ubicacion("Chimiril", "san jose", "almirante antonio - 345 casa 34");
        Cliente clienteDeVenta = new Cliente("Maria", "Luján", "43789234", "lujan_@gmail.com", "3834567821", ubicacionCliente, 57);
        Ubicacion ubicacionPropiedad = new Ubicacion("Ojo de agua", "San Jacinto", "av. torres al 1788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 43.5f, "disponible", 47500f);
        Float precioPactadoDeVenta = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeVenta contrato1 = new ContratoDeVenta(propiedad, precioPactadoDeVenta, true, clienteDeVenta, LocalDate.of(2021, 6, 13));

        LibroDeVentas registroDeVentas = new LibroDeVentas();

        try {
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
        } catch (ContratoDeVentaYaExistenteException e) {
            e.printStackTrace();
        }
        
        //ContratoDeVenta contratoABuscar = null;
        try {
            ContratoDeVenta contratoABuscar = registroDeVentas.buscarContratoDeVentaEnLibro(0006);
        } catch (ContratoDeVentaInexistenteException e) {
            e.printStackTrace();
        }
        //no borrar, porque sino no funciona .joel
        ContratoDeVenta contratoABuscar2 = registroDeVentas.buscarContratoDeVentaEnLibro(1234);
    }

    @Test
    public void verificarEliminacionContratoDeVentaDeRegistro(){
        Float operacion = 10500f;
        Ubicacion ubicacionCliente = new Ubicacion("Chimiril", "san jose", "almirante antonio - 345 casa 34");
        Cliente clienteDeVenta = new Cliente("Maria", "Luján", "43789234", "lujan_@gmail.com", "3834567821", ubicacionCliente, 57);
        Ubicacion ubicacionPropiedad = new Ubicacion("Ojo de agua", "San Jacinto", "av. torres al 1788");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 43.5f, "disponible", 47500f);
        Float precioPactadoDeVenta = (propiedad.getPrecioDePropiedad() / operacion);
        ContratoDeVenta contrato1 = new ContratoDeVenta(propiedad, precioPactadoDeVenta, true, clienteDeVenta, LocalDate.of(2021, 6, 13));

        LibroDeVentas registroDeVentas = new LibroDeVentas();
        
        try {
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
        } catch (ContratoDeVentaYaExistenteException e) {
            e.printStackTrace();
        }

        Integer codigoDeFactura = contrato1.getComprobanteDeVenta().getFacturaDeVenta().getCodigoDeOperacion();
        registroDeVentas.removerContratoDeVentaDeRegistro(codigoDeFactura);

        assertEquals(0, registroDeVentas.getRegistroDeVentas().size());
    }

    @Test
    public void verificarModificacionDeContratoDeVenta(){
        Float operacion = 10500f;
        Ubicacion ubicacionCliente = new Ubicacion("Chimiril", "san jose", "almirante antonio - 345 casa 34");
        Cliente clienteDeVenta = new Cliente("Maria", "Luján", "43789234", "lujan_@gmail.com", "3834567821", ubicacionCliente, 57);
        Ubicacion ubicacionPropiedad = new Ubicacion("Ojo de agua", "San Jacinto", "av. torres al 1788");
        Ubicacion ubicacionPropiedad2 = new Ubicacion("Ojo de agua", "San Jacinto", "av. torres al 3780");
        Propiedad propiedad = new Propiedad(ubicacionPropiedad, 43.5f, "disponible", 47500f);
        Propiedad propiedad2 = new Propiedad(ubicacionPropiedad2, 34.5f, "disponible", 38500f);
        Float precioPactadoDeVenta = (propiedad.getPrecioDePropiedad() / operacion);
        Float precioPactadoDeVenta2 = (propiedad2.getPrecioDePropiedad() / operacion);
        ContratoDeVenta contrato1 = new ContratoDeVenta(propiedad, precioPactadoDeVenta, true, clienteDeVenta, LocalDate.of(2021, 6, 13));
        ContratoDeVenta contrato2 = new ContratoDeVenta(propiedad2, precioPactadoDeVenta2, true, clienteDeVenta, LocalDate.of(2021, 8, 17));

        LibroDeVentas registroDeVentas = new LibroDeVentas();
        
        try {
            registroDeVentas.agregarContratoDeVentaARegistro(contrato1);
        } catch (ContratoDeVentaYaExistenteException e) {
            e.printStackTrace();
        }

        registroDeVentas.modificarContratoDeVenta(contrato1, contrato2);

        assertEquals(contrato2, registroDeVentas.getRegistroDeVentas().get(0));
    }
}
