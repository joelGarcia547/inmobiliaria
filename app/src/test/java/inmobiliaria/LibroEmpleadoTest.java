package inmobiliaria;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import Exceptions.ClienteExistenteEnRegistroException;
import Exceptions.ClienteInexistenteEnRegistroException;

public class LibroEmpleadoTest {
    @Test
    public void testAgregarEmpleadoAlRegistro(){
        Ubicacion ubicacion1 = new Ubicacion("Piedra Blanca", "100 Viviendas", "Casa NÂ°55");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca Capital", "Bancario", "Calle Rosales 424");
        Empleado empleado5 = new Empleado("Maria", "Guzman", "12345678", 
                        "mari@gmail.com", "3834123344", ubicacion1, 
                        "Vendedor");
        Empleado empleado6 = new Empleado("Jose", "Lopez", "87654321", 
                "jose@gmail.com", "3834998877", ubicacion2, 
                "Vendedor");
        LibroEmpleado libroEmpleados = new LibroEmpleado();

        try {
            libroEmpleados.agregarEmpleadoAlRegistro(empleado5);
            libroEmpleados.agregarEmpleadoAlRegistro(empleado6);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        List<Empleado> listaEmpleados = libroEmpleados.getListaDeEmpleados();
        Integer cantidadElementosQueAgregue = 2, cantidadQueMeLlego = listaEmpleados.size();

        assertEquals(cantidadElementosQueAgregue, cantidadQueMeLlego);
    }

    @Test (expected = ClienteExistenteEnRegistroException.class)
    public void testAgregarEmpleadoRepetido() throws ClienteExistenteEnRegistroException{
        Ubicacion ubicacion1 = new Ubicacion("Piedra Blanca", "100 Viviendas", "Casa NÂ°55");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca Capital", "Bancario", "Calle Rosales 424");
        Empleado empleado7 = new Empleado("Maria", "Guzman", "12345678", 
                        "mari@gmail.com", "3834123344", ubicacion1, 
                        "Vendedor");
        Empleado empleado8 = new Empleado("Jose", "Lopez", "12345678", 
                "jose@gmail.com", "3834998877", ubicacion2, 
                "Vendedor");
        LibroEmpleado libroEmpleados = new LibroEmpleado();

            libroEmpleados.agregarEmpleadoAlRegistro(empleado7);
            libroEmpleados.agregarEmpleadoAlRegistro(empleado8);
    }

    @Test
    public void testVerificarEmpleado(){
        Ubicacion ubicacion1 = new Ubicacion("Piedra Blanca", "100 Viviendas", "Casa NÂ°55");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca Capital", "Bancario", "Calle Rosales 424");
        Empleado empleado9 = new Empleado("Maria", "Guzman", "12345678", 
                        "mari@gmail.com", "3834123344", ubicacion1, 
                        "Vendedor");
        Empleado empleado10 = new Empleado("Jose", "Lopez", "87654321", 
                "jose@gmail.com", "3834998877", ubicacion2, 
                "Vendedor");
        LibroEmpleado libroEmpleados = new LibroEmpleado();

        try {
            libroEmpleados.agregarEmpleadoAlRegistro(empleado9);
            libroEmpleados.agregarEmpleadoAlRegistro(empleado10);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        Boolean empleadoExiste = libroEmpleados.verificarEmpleadoEnReg(empleado9.getDNIdeLaPersona());

        assertEquals(true, empleadoExiste);
    }

    @Test
    public void testVerificarEmpleadoFalso(){
        Ubicacion ubicacion1 = new Ubicacion("Piedra Blanca", "100 Viviendas", "Casa NÂ°55");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca Capital", "Bancario", "Calle Rosales 424");
        Empleado empleado11 = new Empleado("Maria", "Guzman", "12345678", 
                        "mari@gmail.com", "3834123344", ubicacion1, 
                        "Vendedor");
        Empleado empleado12 = new Empleado("Jose", "Lopez", "87654321", 
                "jose@gmail.com", "3834998877", ubicacion2, 
                "Vendedor");
        LibroEmpleado libroEmpleados = new LibroEmpleado();

        try {
            libroEmpleados.agregarEmpleadoAlRegistro(empleado11);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        Boolean empleadoFalso = libroEmpleados.verificarEmpleadoEnReg(empleado12.getDNIdeLaPersona());

        assertEquals(false, empleadoFalso);
    }

    @Test
    public void testEliminarEmpleadoDelregistro(){
        Ubicacion ubicacion1 = new Ubicacion("Piedra Blanca", "100 Viviendas", "Casa NÂ°55");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca Capital", "Bancario", "Calle Rosales 424");
        Empleado empleado13 = new Empleado("Maria", "Guzman", "12345678", 
                        "mari@gmail.com", "3834123344", ubicacion1, 
                        "Vendedor");
        Empleado empleado14 = new Empleado("Jose", "Lopez", "87654321", 
                "jose@gmail.com", "3834998877", ubicacion2, 
                "Vendedor");
        LibroEmpleado libroEmpleados = new LibroEmpleado();

        try {
            libroEmpleados.agregarEmpleadoAlRegistro(empleado13);
            libroEmpleados.agregarEmpleadoAlRegistro(empleado14);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        try {
            libroEmpleados.eliminarEmpleadoDelRegistro(empleado13.getNumeroDeLegajo());
            libroEmpleados.eliminarEmpleadoDelRegistro(empleado14.getNumeroDeLegajo());
        } catch (ClienteInexistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        List<Empleado> listaEmpleados = libroEmpleados.getListaDeEmpleados();
        Integer cantidadQueEspero = 0, cantidadQueLlega = listaEmpleados.size();

        assertEquals(cantidadQueEspero, cantidadQueLlega);
    }

    @Test (expected = ClienteInexistenteEnRegistroException.class)
    public void testEliminarEmpleadoInexistente() throws ClienteInexistenteEnRegistroException{
        Ubicacion ubicacion1 = new Ubicacion("Piedra Blanca", "100 Viviendas", "Casa NÂ°55");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca Capital", "Bancario", "Calle Rosales 424");
        Empleado empleado15 = new Empleado("Maria", "Guzman", "12345678", 
                        "mari@gmail.com", "3834123344", ubicacion1, 
                        "Vendedor");
        Empleado empleado16 = new Empleado("Jose", "Lopez", "87654321", 
                "jose@gmail.com", "3834998877", ubicacion2, 
                "Vendedor");
        LibroEmpleado libroEmpleados = new LibroEmpleado();

        try {
            libroEmpleados.agregarEmpleadoAlRegistro(empleado15);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        libroEmpleados.eliminarEmpleadoDelRegistro(empleado16.getNumeroDeLegajo());
    }

    @Test
    public void testBuscarEmpleado(){
        Ubicacion ubicacion1 = new Ubicacion("Piedra Blanca", "100 Viviendas", "Casa NÂ°55");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca Capital", "Bancario", "Calle Rosales 424");
        Empleado empleado17 = new Empleado("Maria", "Guzman", "12345678", 
                        "mari@gmail.com", "3834123344", ubicacion1, 
                       "Vendedor");
        Empleado empleado18 = new Empleado("Jose", "Lopez", "87654321", 
                "jose@gmail.com", "3834998877", ubicacion2, 
                "Vendedor");
        LibroEmpleado libroEmpleados = new LibroEmpleado();

        try {
            libroEmpleados.agregarEmpleadoAlRegistro(empleado17);
            libroEmpleados.agregarEmpleadoAlRegistro(empleado18);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        Empleado empleadoQueEspero = empleado17;
        Empleado empleadoQueLlega = null;

        try {
            empleadoQueLlega = libroEmpleados.buscarEmpleadoEnRegistro(empleado17.getNumeroDeLegajo());
        } catch (ClienteInexistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(empleadoQueEspero, empleadoQueLlega);
    }

    @Test (expected = ClienteInexistenteEnRegistroException.class)
    public void testBuscarEmpleadoInexistente() throws ClienteInexistenteEnRegistroException{
        Ubicacion ubicacion1 = new Ubicacion("Piedra Blanca", "100 Viviendas", "Casa NÂ°55");
        Ubicacion ubicacion2 = new Ubicacion("Catamarca Capital", "Bancario", "Calle Rosales 424");
        Empleado empleado18 = new Empleado("Maria", "Guzman", "12345678", 
                        "mari@gmail.com", "3834123344", ubicacion1, 
                        "Vendedor");
        Empleado empleado19 = new Empleado("Jose", "Lopez", "87654321", 
                "jose@gmail.com", "3834998877", ubicacion2, 
                "Vendedor");
        LibroEmpleado libroEmpleados = new LibroEmpleado();

        try {
            libroEmpleados.agregarEmpleadoAlRegistro(empleado18);
            //libroEmpleados.agregarEmpleadoAlRegistro(empleado2);
        } catch (ClienteExistenteEnRegistroException e) {
            System.out.println(e.getMessage());
        }

        libroEmpleados.buscarEmpleadoEnRegistro(empleado19.getNumeroDeLegajo());
    }
}
