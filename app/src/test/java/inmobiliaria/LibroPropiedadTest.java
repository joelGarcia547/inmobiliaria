package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Exceptions.PropiedadInexistenteException;
import Exceptions.PropiedadRepetidaException;

public class LibroPropiedadTest {
    @Test
    public void verificarAgregadoDePropiedadALibro(){
        Ubicacion ubicacion = new Ubicacion("Capital", "Ojo de agua", "felipe gonzales - altura 456");
        Propiedad propiedad = new Propiedad(ubicacion, 40f, "Disponible", 60000f);
        LibroPropiedad libro = new LibroPropiedad();

        try {
            libro.agregarPropiedadALibro(propiedad);
        } catch (PropiedadRepetidaException e) {
            System.out.println(e.getMessage());
            
        }
        assertEquals(1, libro.getRegistroDePropiedades().size());
    }
    @Test (expected = PropiedadRepetidaException.class)
    public void testAgregarPropiedadRepetida()throws PropiedadRepetidaException {
        Ubicacion ubicacion1 = new Ubicacion("Capital", "Ojo de agua", "felipe gonzales - altura 456");
        Ubicacion ubicacion2 = new Ubicacion("Andalgala", "100vv", "Casa N°55");
        Ubicacion ubicacion3 = ubicacion1;
        Propiedad propiedad1 = new Propiedad(ubicacion1, 40f, "Disponible", 60000f);
        Propiedad propiedad2 = new Propiedad(ubicacion2, 40f, "Alquilada", 60000F);
        Propiedad propiedad3 = new Propiedad(ubicacion3, 45F, "Disponible", 60000F);
        LibroPropiedad libro = new LibroPropiedad();

        try {
            libro.agregarPropiedadALibro(propiedad1);
            libro.agregarPropiedadALibro(propiedad2);
        } catch (PropiedadRepetidaException e) {
            System.out.println(e.getMessage());
        }
        libro.agregarPropiedadALibro(propiedad3);
    }

    @Test
    public void testVerificarPropiedad(){
        Ubicacion ubicacion1 = new Ubicacion("Capital", "Ojo de agua", "felipe gonzales - altura 456");
        Ubicacion ubicacion2 = new Ubicacion("Andalgala", "100vv", "Casa N°55");
        Propiedad propiedad1 = new Propiedad(ubicacion1, 40f, "Disponible", 60000f);
        Propiedad propiedad2 = new Propiedad(ubicacion2, 40f, "Alquilada", 60000F);
        LibroPropiedad libro = new LibroPropiedad();

        try {
            libro.agregarPropiedadALibro(propiedad1);
            libro.agregarPropiedadALibro(propiedad2);
        } catch (PropiedadRepetidaException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(true, libro.verificarPropiedad(propiedad2));
    }

    @Test
    public void testVerificarPropiedadInexistente(){
        Ubicacion ubicacion1 = new Ubicacion("Capital", "Ojo de agua", "felipe gonzales - altura 456");
        Ubicacion ubicacion2 = new Ubicacion("Andalgala", "100vv", "Casa N°55");
        Propiedad propiedad1 = new Propiedad(ubicacion1, 40f, "Disponible", 60000f);
        Propiedad propiedad2 = new Propiedad(ubicacion2, 40f, "Alquilada", 60000F);
        LibroPropiedad libro = new LibroPropiedad();

        try {
            libro.agregarPropiedadALibro(propiedad1);
        } catch (PropiedadRepetidaException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(false, libro.verificarPropiedad(propiedad2));
    }

    @Test
    public void testBusquedaDePropiedadEnLibro(){
        Ubicacion ubicacion1 = new Ubicacion("Capital", "Ojo de agua", "felipe gonzales - altura 456");
        Ubicacion ubicacion2 = new Ubicacion("Andalgala", "100vv", "Casa N°55");
        Propiedad propiedad1 = new Propiedad(ubicacion1, 40f, "Disponible", 60000f);
        Propiedad propiedad2 = new Propiedad(ubicacion2, 40f, "Alquilada", 60000F);
        LibroPropiedad libro = new LibroPropiedad();

        try {
            libro.agregarPropiedadALibro(propiedad1);
            libro.agregarPropiedadALibro(propiedad2);
        } catch (PropiedadRepetidaException e) {
            System.out.println(e.getMessage());
        }

        Propiedad propiedadQueEspero = null;
        try {
            propiedadQueEspero = libro.buscarPropiedadEnLibro(ubicacion2);
        } catch (PropiedadInexistenteException e) {
            System.out.println(e.getMessage());
        }

        assertEquals(propiedad2, propiedadQueEspero);
    }
    @Test (expected = PropiedadInexistenteException.class)
    public void testBuscarPropiedadInexistente() throws PropiedadInexistenteException{
        Ubicacion ubicacion1 = new Ubicacion("Capital", "Ojo de agua", "felipe gonzales - altura 456");
        Ubicacion ubicacion2 = new Ubicacion("Andalgala", "100vv", "Casa N°55");
        Propiedad propiedad1 = new Propiedad(ubicacion1, 40f, "Disponible", 60000f);
        Propiedad propiedad2 = new Propiedad(ubicacion2, 40f, "Alquilada", 60000F);
        LibroPropiedad libro = new LibroPropiedad();

        try {
            libro.agregarPropiedadALibro(propiedad1);
            
        } catch (PropiedadRepetidaException e) {
            System.out.println(e.getMessage());
        }
        libro.buscarPropiedadEnLibro(ubicacion2);
    }

    @Test
    public void verificarEliminacionDePropiedadDelLibro(){
        Ubicacion ubicacion1 = new Ubicacion("Capital", "Ojo de agua", "felipe gonzales - altura 456");
        Ubicacion ubicacion2 = new Ubicacion("Andalgala", "100vv", "Casa N°55");
        Propiedad propiedad1 = new Propiedad(ubicacion1, 40f, "Disponible", 60000f);
        Propiedad propiedad2 = new Propiedad(ubicacion2, 40f, "Alquilada", 60000F);
        LibroPropiedad libro = new LibroPropiedad();

        try {
            libro.agregarPropiedadALibro(propiedad1);
            libro.agregarPropiedadALibro(propiedad2);
        } catch (PropiedadRepetidaException e) {
            System.out.println(e.getMessage());
        }

        Integer cantidadDeElementosQueLlegan = 0, cantidadDeElementosQueEspero = 1;
        try {
            libro.eliminarPropiedadDelLibro(propiedad2);
        } catch (PropiedadInexistenteException e) {
            System.out.println(e.getMessage());
        }
        cantidadDeElementosQueLlegan = libro.getRegistroDePropiedades().size();
        assertEquals(cantidadDeElementosQueEspero, cantidadDeElementosQueLlegan);
    }
    @Test (expected = PropiedadInexistenteException.class)
    public void testEliminarPropiedadInexistente() throws PropiedadInexistenteException{
        Ubicacion ubicacion1 = new Ubicacion("Capital", "Ojo de agua", "felipe gonzales - altura 456");
        Ubicacion ubicacion2 = new Ubicacion("Andalgala", "100vv", "Casa N°55");
        Propiedad propiedad1 = new Propiedad(ubicacion1, 40f, "Disponible", 60000f);
        Propiedad propiedad2 = new Propiedad(ubicacion2, 40f, "Alquilada", 60000F);
        LibroPropiedad libro = new LibroPropiedad();

        try {
            libro.agregarPropiedadALibro(propiedad1);
        } catch (PropiedadRepetidaException e) {
            System.out.println(e.getMessage());
        }
        libro.eliminarPropiedadDelLibro(propiedad2);
    }
}
