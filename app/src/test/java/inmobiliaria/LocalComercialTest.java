package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LocalComercialTest {
    @Test
    public void testMetrosCuadradosDeVidriera(){
        Ubicacion ubicacionDelLocal = new Ubicacion("Catamarca Capital", 
        "Centro", "Republica 1000");
        LocalComercial localComercialAuxiliar = new LocalComercial(3F, true, false, 
                                               ubicacionDelLocal, 8F, "disponible", 40000F);
        
        Float metrosCuadradosDeLaVidrieraAuxiliar = 3F;
        
        assertEquals(metrosCuadradosDeLaVidrieraAuxiliar, localComercialAuxiliar.getMetrosCuadradosDeVidriera());;
    }

    @Test
    public void testPoseeBanioElLocal(){
        Ubicacion ubicacionDelLocal = new Ubicacion("Catamarca Capital", 
        "Centro", "Republica 1000");
        LocalComercial localComercialAuxiliar = new LocalComercial(3F, true, false, 
                                               ubicacionDelLocal, 8F, "disponible", 40000F);
        

        assertEquals(true, localComercialAuxiliar.getPoseeBanioElLocal());
    }

    @Test
    public void testPoseeCocinaElLocal(){
        Ubicacion ubicacionDelLocal = new Ubicacion("Catamarca Capital", 
        "Centro", "Republica 1000");
        LocalComercial localComercialAuxiliar = new LocalComercial(3F, true, false, 
                                               ubicacionDelLocal, 8F, "disponible", 40000F);
        

        assertEquals(false, localComercialAuxiliar.getPoseeCocinaElLocal());
    }
}
