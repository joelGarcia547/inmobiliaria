package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PropiedadTest {
    @Test
    public void testMetrosCuadradosDeLaPropiedad(){
        //Propiedad propiedadAuxiliar = new Propiedad();
        Ubicacion ubicacion = new Ubicacion("Catamarca Capital", "Ojo de Agua", 
        "Cabildo abierto 555");
        Casa casaAuxiliar = new Casa(3, ubicacion, 28.5f, "alquilada", 7800f);

        assertEquals(28.5f, casaAuxiliar.getMetrosCuadradosDeLaPropiedad(),0.5f);
    }

}













