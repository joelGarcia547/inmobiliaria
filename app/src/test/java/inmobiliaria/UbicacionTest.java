package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UbicacionTest {
    
    @Test
    public void verificarRetornoToString(){

        //Ubicacion ubicacion = new Ubicacion(); 
        Ubicacion ubicacion2 = new Ubicacion("San isidro", "Los alamos", "Guillermo correa - 1400");
        String mensaje = ubicacion2.toString();  //ejecuto lo que voy a probar y me quedo con el valor
        
        assertEquals("Mi ubicacion", mensaje); 
    }
}
